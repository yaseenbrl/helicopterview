// Handles the switch between the different modes
// Handles document events
class MasterController {

  static drawingCanvas = null;
  static drawingToolbarCanvas = null;
  static mzdMainCanvas = null;
  static mzdToolbarCanvas = null;

  static interactiveCanvas = null;
  static interactiveToolbarCanvas = null;
  static dynamicZoomCanvas = null;
  static mzdMainCanvas = null;

  static mode = null;
  static clipboard = null;
  static canvasInAction = null;

  static zoomViews = [];



  static broadcastChannel = new BroadcastChannel('staticZoomsChannel');

  static switchTo(mode, _callback) {
    switch (mode) {
      case 'INTERACTIVE':
        MasterController.switchToInteractiveMode()
        break;
      case 'DRAWING':
        MasterController.switchToDrawingMode()
        break;
      case 'MULTI-ZOOM-DEFINITION':
        MasterController.switchToMultiZoomDefinitionMode()
        break;
      default:

    }
    _callback()
  }
  static switchToInteractiveMode() {
    var drawingMode_div = document.getElementsByClassName("drawingMode")[0];
    // var interactiveMode_div = document.getElementsByClassName("interactiveMode")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_div = document.getElementsByClassName("multiZoomDefinitionMode")[0];

    var drawingMode_btn = document.getElementsByName("drawingMode_btn")[0];
    // var interactiveMode_btn = document.getElementsByName("interactiveMode_btn")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_btn = document.getElementsByName("multiZoomDefinitionMode_btn")[0];

    drawingMode_div.style.display = "none"
    // interactiveMode_div.style.display = "block" // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_div.style.display = "none"

    drawingMode_btn.style.color = "#000";
    drawingMode_btn.style.backgroundColor = "#fff"
    // interactiveMode_btn.style.color = Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE; //"#0c9fdc"// INTERACTIVE_MODE_RELATED
    // interactiveMode_btn.style.backgroundColor = "#eee" // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_btn.style.color = "#000"
    multiZoomDefinitionMode_btn.style.backgroundColor = "#fff"

    IM_controller.init()
    MasterController.mode = 'INTERACTIVE'
    MasterController.canvasInAction = MasterController.interactiveCanvas
    StatsBarController.render(MasterController.mode)


  }

  static setInfoBar(txt) {
    document.getElementById("infoBar").innerHTML = txt
  }
  static switchToDrawingMode() {
    var drawingMode_div = document.getElementsByClassName("drawingMode")[0];
    // var interactiveMode_div = document.getElementsByClassName("interactiveMode")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_div = document.getElementsByClassName("multiZoomDefinitionMode")[0];

    var drawingMode_btn = document.getElementsByName("drawingMode_btn")[0];
    // var interactiveMode_btn = document.getElementsByName("interactiveMode_btn")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_btn = document.getElementsByName("multiZoomDefinitionMode_btn")[0];

    drawingMode_div.style.display = "flex";
    // interactiveMode_div.style.display = "none"; // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_div.style.display = "none";

    drawingMode_btn.style.color = Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE; //"#0c9fdc";
    drawingMode_btn.style.backgroundColor = "#eee"
    // interactiveMode_btn.style.color = "#000"; // INTERACTIVE_MODE_RELATED
    // interactiveMode_btn.style.backgroundColor = "#fff" // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_btn.style.color = "#000";
    multiZoomDefinitionMode_btn.style.backgroundColor = "#fff"


    DM_controller.init()
    MasterController.mode = 'DRAWING'
    MasterController.canvasInAction = MasterController.drawingCanvas
    StatsBarController.render(MasterController.mode)

  }

  static switchToMultiZoomDefinitionMode() {
    var drawingMode_div = document.getElementsByClassName("drawingMode")[0];
    // var interactiveMode_div = document.getElementsByClassName("interactiveMode")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_div = document.getElementsByClassName("multiZoomDefinitionMode")[0];

    var drawingMode_btn = document.getElementsByName("drawingMode_btn")[0];
    // var interactiveMode_btn = document.getElementsByName("interactiveMode_btn")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_btn = document.getElementsByName("multiZoomDefinitionMode_btn")[0];

    drawingMode_div.style.display = "none";
    // interactiveMode_div.style.display = "none"; // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_div.style.display = "flex";

    drawingMode_btn.style.color = "#000";
    drawingMode_btn.style.backgroundColor = "#fff"
    // interactiveMode_btn.style.color = "#000"; // INTERACTIVE_MODE_RELATED
    // interactiveMode_btn.style.backgroundColor = "#fff" // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_btn.style.color = Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE; //"#0c9fdc";
    multiZoomDefinitionMode_btn.style.backgroundColor = "#eee"

    MM_controller.init()
    MasterController.mode = 'MULTI-ZOOM-DEFINITION';
    MasterController.canvasInAction = MasterController.mzdMainCanvas
    StatsBarController.render(MasterController.mode)

  }

  static postMessage(object) {
    var _json = JSON.stringify(object);
    var compressed_json = LZString.compress(_json);
    MasterController.broadcastChannel.postMessage(compressed_json)

  }

  static addDocumentEventListners() {


    // INCREDIBLY DIRTY CODE STARTS
    var drawingMode_btn = document.getElementsByName("drawingMode_btn")[0];
    // var interactiveMode_btn = document.getElementsByName("interactiveMode_btn")[0]; // INTERACTIVE_MODE_RELATED
    var multiZoomDefinitionMode_btn = document.getElementsByName("multiZoomDefinitionMode_btn")[0];
    drawingMode_btn.addEventListener('mouseenter', e => {
      drawingMode_btn.style.backgroundColor = "#eee"
    });
    // interactiveMode_btn.addEventListener('mouseenter', e => { // INTERACTIVE_MODE_RELATED
    // interactiveMode_btn.style.backgroundColor = "#eee" // INTERACTIVE_MODE_RELATED
    // }); // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_btn.addEventListener('mouseenter', e => {
      multiZoomDefinitionMode_btn.style.backgroundColor = "#eee"
    });
    drawingMode_btn.addEventListener('mouseleave', e => {
      if (drawingMode_btn.style.color !== "rgb(12, 159, 220)") {
        drawingMode_btn.style.backgroundColor = "#fff"
      }
    });
    // interactiveMode_btn.addEventListener('mouseleave', e => { // INTERACTIVE_MODE_RELATED
    // if (interactiveMode_btn.style.color !== "rgb(12, 159, 220)") { // INTERACTIVE_MODE_RELATED
    // interactiveMode_btn.style.backgroundColor = "#fff" // INTERACTIVE_MODE_RELATED
    // } // INTERACTIVE_MODE_RELATED
    // }); // INTERACTIVE_MODE_RELATED
    multiZoomDefinitionMode_btn.addEventListener('mouseleave', e => {
      if (multiZoomDefinitionMode_btn.style.color !== "rgb(12, 159, 220)") {
        multiZoomDefinitionMode_btn.style.backgroundColor = "#fff"
      }
    });
    // INCREDIBLY DIRTY CODE kinda ENDS


    switch (MasterController.mode) {



      case 'MULTI-ZOOM-DEFINITION':
        console.log("-DEFINITION'");



        break;



      case 'DRAWING':

        document.addEventListener("keydown", event => {
          // var ctrlDown = false
          // if (event.keyCode == 93 || event.keyCode == 17) ctrlDown = true;
          console.log(event)
          if (event.keyCode == 27) { // ESC

            DM_controller.drawPolygonMode = false
            DM_controller.drawTileMode = false
            DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.finalPolygon)
            DM_drawingActionsHandler.finalPolygon = null;
            DM_drawingActionsHandler.polygonPoints = [];
            DM_drawingActionsHandler.pointers.forEach(a => {
              DM_controller.drawingCanvas.remove(a);
            });
            DM_drawingActionsHandler.pointers = [];
            MasterController.drawingCanvas.discardActiveObject().requestRenderAll();
            MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();

          }
          if (event.keyCode == 8) { // del

            var obj = MasterController.drawingCanvas.getActiveObject();
            if (obj) {
              if (obj.type === "activeSelection") {
                obj._objects.forEach(a => {
                  MasterController.drawingCanvas.remove(a);
                });
              } else {
                MasterController.drawingCanvas.remove(MasterController.drawingCanvas.getActiveObject());
              }
              MasterController.drawingCanvas.discardActiveObject().requestRenderAll();
            }
          }

          if (event.keyCode == 90 && (event.ctrlKey || event.metaKey) && (!event.altKey)) { //ctrl z
            DM_commandsController.undo()
          }
          if (event.keyCode == 89 && (event.ctrlKey || event.metaKey) && (!event.altKey)) { //ctrl y
            DM_commandsController.redo()
          }
          if (event.keyCode == 67 && (event.ctrlKey || event.metaKey) && (!event.altKey)) { //ctrl c
            MasterController.copy()
          }
          if (event.keyCode == 86 && (event.ctrlKey || event.metaKey) && (!event.altKey)) { //ctrl v
            MasterController.paste(false)
          }
          if (event.keyCode == 86 && (event.ctrlKey || event.metaKey) && (event.altKey)) { //ctrl alt v
            DM_drawingActionsHandler.align('left')
          }
          if (event.keyCode == 72 && (event.ctrlKey || event.metaKey) && (event.altKey)) { //ctrl alt h
            DM_drawingActionsHandler.align('top')
          }
          if (event.keyCode == 48 && (event.ctrlKey || event.metaKey) && (event.altKey)) { //ctrl alt 0
            DM_drawingActionsHandler.snapAngle(0)
          }
          if (event.keyCode == 90 && (!((event.ctrlKey || event.metaKey)))) { //z
            var objs = MasterController.canvasInAction._objects.filter(_ => _.type !== 'image');
            objs.forEach(o => {
              o.set({
                old_opacity: o.old_opacity ? Math.max(o.old_opacity, o.opacity) : o.opacity
              })
            });
            objs.forEach(o => {
              o.set({
                opacity: 0
              })
            })
            MasterController.canvasInAction.requestRenderAll()
          }

        })


        document.addEventListener("keyup", event => {

          if (event.keyCode == 90 && (!((event.ctrlKey || event.metaKey)))) { //z
            var objs = MasterController.canvasInAction._objects.filter(_ => _.type !== 'image');
            objs.forEach(o => {
              o.set({
                opacity: o.old_opacity
              })
            })
            MasterController.canvasInAction.requestRenderAll()
          }


        })



        break;



      case 'INTERACTIVE':
        console.log("'INTERACTIVE'");



        break;
      default:

    }
  }

  static getFileContent(_callback) {
    var input = document.createElement('input');
    input.type = "file";
    input.onchange = async function(e) {
      var file = input.files[0];
      if (file) {
        var reader = new FileReader();

        reader.readAsText(file, "UTF-8");
        reader.onload = function(evt) {
          _callback(evt.target.result)
          return evt.target.result
        }
        reader.onerror = function(evt) {
          alert("error reading file");
          return null
        }
      }
    }
    $(input).click()

  }


  static deepCopy(object) {
    return JSON.parse(JSON.stringify(object))
  }


  static copy() {
    console.log("COPIED .. !! ")
    var canvas = MasterController.canvasInAction

    // clone what are you copying since you
    // may want copy and paste on different moment.
    // and you do not want the changes happened
    // later to reflect on the copy.
    canvas.getActiveObject().clone(function(cloned) {
      if (cloned.type === 'activeSelection') {

        //loop through the active objects and map metaData
        var original_objects = MasterController.canvasInAction.getActiveObject()._objects
        var i = 0
        cloned.forEachObject(function(obj) {
          obj.set({
            meta: JSON.parse(JSON.stringify(original_objects[i].meta))
          })
          i += 1
        });

      } else {

        cloned.set({
          meta: JSON.parse(JSON.stringify(canvas.getActiveObject().meta))
        })


      }

      MasterController.clipboard = cloned;

    });
  }

  static paste(sameLocation = false) {
    console.log("PASTED .. !! ")

    var canvas = MasterController.canvasInAction
    // clone again, so you can do multiple copies.
    var clipboard_obj = MasterController.clipboard._objects ? MasterController.clipboard._objects : MasterController.clipboard


    MasterController.clipboard.clone(function(clonedObj) {
      canvas.discardActiveObject();
      clonedObj.set({
        left: sameLocation ? clonedObj.left : clonedObj.left + 10,
        top: sameLocation ? clonedObj.top : clonedObj.top + 10,
      });
      if (clonedObj.type === 'activeSelection') {
        // active selection needs a reference to the canvas.
        clonedObj.canvas = canvas;
        var i = 0
        clonedObj.forEachObject(function(obj) {
          canvas.add(obj);
          obj.set({
            meta: JSON.parse(JSON.stringify(clipboard_obj[i].meta))
          })
          i += 1;
        });

        // this should solve the unselectability
        clonedObj.setCoords();
      } else {
        canvas.add(clonedObj);
        clonedObj.set({
          meta: JSON.parse(JSON.stringify(clipboard_obj.meta))
        })
      }
      MasterController.clipboard.top += 10;
      MasterController.clipboard.left += 10;
      canvas.setActiveObject(clonedObj);
      canvas.requestRenderAll();
    });
  }

  static saveToFile(_text, extension = 'json', prefix = 'yard') {
    var dataStr = "data:text/" + extension + ";charset=utf-8," + encodeURIComponent(_text);
    var dlAnchorElem = document.createElement('a');
    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", prefix + "-" + new Date().getTime() + "." + extension);
    dlAnchorElem.click();

  }

  static generateIDs(start_id, num) {


    function getLetters(text) {
      return text.replace(/[^A-Za-z]/g, '');
    }

    function getNumbers(text) {
      return text.replace(/[^0-9]/g, '');
    }

    if (/([a-zA-Z]*[0-9]+)/.test(start_id) == false) {
      alert("Can't populate this pattern");
      return null;
    }
    var length = start_id.length
    var letters = getLetters(start_id);
    // if (letters !== getLetters(end_id)) {
    //   alert("Can't populate this pattern");
    //   return null;
    // }
    var start_number = parseInt(getNumbers(start_id))
    // var end_number = parseInt(getNumbers(end_id))
    var IDs = new Array();
    console.log("Generating (" + letters + ") and numbers between " + start_number + " - " + num)
    for (var i = start_number; i < start_number + num; i++) {
      var zeros_length = length - (i).toString().length - letters.length
      IDs.push(letters + "0".repeat(zeros_length) + i)
    }
    console.log("Generated: " + IDs)
    return IDs

  }

  static _showIDs() {

    var color = MasterController.mode == 'INTERACTIVE' ? 'black' : 'white';
    var objects = MasterController.canvasInAction._objects;
    var toBeAdded = [];
    var toBeRemoved = [];

    for (var i = 0; i < objects.length; i++) {
      if (objects[i].type === 'text') {
        toBeRemoved.push(objects[i])
      }
    }

    for (var i = 0; i < toBeRemoved.length; i++) {
      MasterController.canvasInAction.remove(toBeRemoved[i]);
    }

    for (var i = 0; i < objects.length; i++) {

      //discard objects that are selected .. prevents awkward rendering of text objects
      if (MasterController.canvasInAction.getActiveObject() &&
        MasterController.canvasInAction.getActiveObject()._objects &&
        MasterController.canvasInAction.getActiveObject()._objects.includes(objects[i])) {
        continue
      }

      var center_point = objects[i].getCenterPoint();

      if (objects[i].meta && objects[i].meta.id) {
        if (objects[i].meta.id.includes('zoom') || objects[i].meta.id.includes('view')) {
          continue;
        }

        var textObj = new fabric.Text(objects[i].meta.id, {
          fill: color,
          fontSize: Math.min(22, 0.25 * Math.max(objects[i].width, objects[i].height)),
          left: center_point.x,
          top: center_point.y,
          originX: 'center',
          originY: 'center',
          selectable: false,
          evented: false,
          angle: (objects[i].angle - 90) < -90 ? 180 + (objects[i].angle - 90) : objects[i].angle - 90,
          fontFamily: Constants.FONT_FAMILY,
        })



        toBeAdded.push(textObj)
      }
    }


    for (var i = 0; i < toBeAdded.length; i++) {
      MasterController.canvasInAction.add(toBeAdded[i]);
    }


    //Bad code due to new requirements
    MasterController.colorObjects()
  }
  static _showIDs_beta() {

    var color = MasterController.mode == 'INTERACTIVE' ? 'black' : 'white';
    var objects = MasterController.canvasInAction._objects;
    var toBeAdded = [];
    var toBeRemoved = [];

    for (var i = 0; i < objects.length; i++) {
      if (objects[i].type === 'text') {
        toBeRemoved.push(objects[i])
      }
    }

    for (var i = 0; i < toBeRemoved.length; i++) {
      MasterController.canvasInAction.remove(toBeRemoved[i]);
    }

    for (var i = 0; i < objects.length; i++) {

      //discard objects that are selected .. prevents awkward rendering of text objects
      if (MasterController.canvasInAction.getActiveObject() &&
        MasterController.canvasInAction.getActiveObject()._objects &&
        MasterController.canvasInAction.getActiveObject()._objects.includes(objects[i])) {
        continue
      }

      // var center_point = objects[i].getCenterPoint();
      var center_point = objects[i].aCoords.bl

      if (objects[i].meta && objects[i].meta.id) {
        if (objects[i].meta.id.includes('zoom') || objects[i].meta.id.includes('view')) {
          continue;
        }

        var textObj = new fabric.Text(objects[i].meta.id, {
          fill: color,
          fontSize: Math.min(22, 0.20 * Math.max(objects[i].width, objects[i].height)),
          left: center_point.x,
          top: center_point.y,
          originX: 'left',
          originY: 'top',
          selectable: false,
          evented: false,
          angle: (objects[i].angle - 90) < -90 ? 180 + (objects[i].angle - 90) : objects[i].angle - 90,
          fontFamily: Constants.FONT_FAMILY,
        })

        if (objects[i].info && objects[i].info.unit && objects[i].info.unit.num) {
          var center_point = objects[i].aCoords.br


          var unitNumTxt = new fabric.Text(objects[i].info.unit.num.toString(), {
            fill: color,
            fontSize: Math.min(20, 0.15 * Math.max(objects[i].width, objects[i].height)),
            left: center_point.x - 2,
            top: center_point.y - 2,
            originX: 'left',
            originY: 'bottom',
            selectable: false,
            evented: false,
            angle: (objects[i].angle - 90) < -90 ? 180 + (objects[i].angle - 90) : objects[i].angle - 90,
            fontFamily: Constants.FONT_FAMILY,
          })
          toBeAdded.push(unitNumTxt)

        }

        toBeAdded.push(textObj)
      }
    }


    for (var i = 0; i < toBeAdded.length; i++) {
      MasterController.canvasInAction.add(toBeAdded[i]);
    }

  }

  static getPoint(optsions) {

    var canvas = MasterController.canvasInAction
    var offset = canvas._offset;
    var x = (optsions.e.pageX - canvas.viewportTransform[4] - offset.left) * (1 / canvas.viewportTransform[0]);
    var y = (optsions.e.pageY - canvas.viewportTransform[5] - offset.top) * (1 / canvas.viewportTransform[3]);


    return new Point(x, y);
  }

  static hideSelectionBox() {
    var canvas = MasterController.canvasInAction
    canvas.selectionColor = 'rgba(0,0,0,0)';
    canvas.selectionBorderColor = 'rgba(0,0,0,0)';
    canvas.selectionLineWidth = 0;
  }
  static showSelectionBox() {
    var canvas = MasterController.canvasInAction
    canvas.selectionColor = 'rgba(220,40,20,0.5)';
    canvas.selectionBorderColor = 'rgba(220,220,220,0.8)';
    canvas.selectionLineWidth = 1;
  }

  static exportToTable() {
    var relevant_objects = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id)
    var txt = "id,spotType,loadingTypes,group,order,section\n"
    relevant_objects.forEach((o) => {
      var spotType = o.meta.spotType ? o.meta.spotType : '-'
      var loadTypes = o.meta.loadTypes ? o.meta.loadTypes : '-'
      loadTypes = loadTypes.length == 0 ? '-' : loadTypes
      var groupNumber = o.meta.groupNumber ? o.meta.groupNumber : '-'
      var order = o.meta.order ? o.meta.order : '-'
      var section = o.meta.section ? o.meta.section : '-'

      txt += (o.meta.id.toString() + ",")
      txt += (spotType.toString() + ",")
      txt += (loadTypes.toString() + ",")
      txt += (groupNumber.toString() + ",")
      txt += (order.toString() + ",")
      txt += (section.toString() + "\n")


    })

    MasterController.saveToFile(txt, 'csv', 'spotsExport')

  }

  static dec2hex(dec) {
    return dec.toString(16).padStart(2, "0")
  }

  // generateId :: Integer -> String
  static generateId(len) {
    var arr = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, MasterController.dec2hex).join('')
  }


  static colorObjects() {
    let relevant_objects = MasterController.canvasInAction._objects.filter(o => o.type != 'text' && o.type != 'image')
    relevant_objects.forEach((o) => {
      let fill_color = Constants.COLORS.STATIC;
      let font_color = 'white'
      // if (o.meta){
      //   fill_color = Constants.COLORS.STATIC
      // }
      if (o.meta.id && !o.meta.spotType) {
        fill_color = Constants.COLORS.ID_ASSIGNED
        font_color = 'black'
      }

      if (o.meta.id && o.meta.spotType && !o.meta.order) {
        fill_color = Constants.COLORS.CAPABILITIES_ASSIGNED
        font_color = 'black'
      }

      if (o.meta.id && o.meta.spotType && o.meta.order) {
        fill_color = Constants.COLORS.GROUP_ASSIGNED
        font_color = 'white'
      }

      o.set({
        fill: fill_color,
      })

      var text_obj = MasterController.canvasInAction._objects.filter(a => a.text).filter(b => b.text === (o.meta.id))[0]

      if (text_obj)
        text_obj.set({
          fill: font_color
        })

    })
    MasterController.canvasInAction.requestRenderAll();
  }




}