class DialogBoxHandler {


  static showDialogBox(title, actions) {
    var body = document.body
    var dialog_html = `<div id= "dialogWrapper">
    <dialog id = "dialogBox" open>`
    dialog_html += "<p>" + title + "</p>"
    actions.forEach(o => {
      dialog_html += `<input type="button" name="" value="` + o.name + `" onclick="` + o.handler + `">`
    })
    dialog_html += `
      <input type="button" style="margin-top:30px" name="Exit" value="Cancel" onclick="document.getElementById('dialogWrapper').remove()">
    </dialog>
  </div>`
    body.insertAdjacentHTML('beforeend', dialog_html);
  }

}