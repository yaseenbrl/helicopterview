class Constants {
  static COLORS = {
    'zoomViews': ['red', 'green', 'blue',  'orange', 'black', 'cyan', 'violate', 'yellow','#F08080', '#52BE80', '#DC7633'],
    'STATIC': '#607d8b',
    'AVAILABLE': '#f5f5f5',
    'OCCUPIED': '#e77461',
    'CLOSED': '#e5e5e5',
    'BOOKED': '#dbdbdb',
    'BACKGROUND': '#f0f0f0',
    'BORDER_DARK': '#57423F',
    'UNAVAILABLE': 'd2d3d4',
    'UNIT_UNLOADING': '#7d46d7',
    'UNIT_LOADING': '#f4b235',
    'viewRectangle': '#444',
    'groupedSpot': '#00a',
    'CANVAS_BACKGROUND': '#eee',
    'TOOLBAR_BACKGROUND': '#fff',
    'TOOLBAR_RECT_BACKGROUND_INACTIVE': '#4c7fb0',
    'TOOLBAR_RECT_BACKGROUND_ACTIVE': '#4aaaee',
    'TOOLBAR_TEXT_COLOR': '#000',
    'ID_ASSIGNED': '#FFFF99',
    'CAPABILITIES_ASSIGNED': '#61E876',
    'GROUP_ASSIGNED': '#4375a5',
  };

  static INACTIVE_STROKE_COLOR = 'black';
  static ACTIVE_STROKE_COLOR = 'red';
  static INACTIVE_STROKE_WIDTH = 0.7;
  static ACTIVE_STROKE_WIDTH = 3;
  static MAX_ALLOWED_ZOOMVIEWS = 5;
  static FONT_FAMILY = 'PT Sans';
}