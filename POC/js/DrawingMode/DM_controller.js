class DM_controller {

  static drawingCanvas = null;
  static drawingToolbarCanvas = null;

  static drawPolygonMode = false;
  static drawTileMode = false;

  static populateOnTheFly = false;

  static groupsCounter = 0;
  static _orderedSelection = false;
  static _currentSelectionOrder = 1; // represents the order of the last added element to the selection
  static _oldSelection = null;
  static selectionOrder = {}; //represents the order of the selection of the current items


  static init() {

    var containterWidth = document.getElementsByClassName("drawingMode")[0].offsetWidth - 50; // subtract toolbar width
    var containterHeight = document.getElementsByClassName("drawingMode")[0].offsetHeight;

    if (MasterController.drawingCanvas === null) {
      MasterController.addDocumentEventListners()
      MasterController.drawingCanvas = new fabric.Canvas('drawingCanvas');
      MasterController.drawingCanvas.backgroundColor = Constants.COLORS.CANVAS_BACKGROUND;
      MasterController.drawingCanvas.setHeight(containterHeight);
      MasterController.drawingCanvas.setWidth(containterWidth);
      MasterController.drawingCanvas.requestRenderAll();
      DM_controller.drawingCanvas = MasterController.drawingCanvas

      MasterController.drawingCanvas.on('mouse:wheel', DM_viewActionsHandler.zoom_wheel);
      MasterController.drawingCanvas.on('mouse:down', DM_viewActionsHandler.pan_mousedown);
      MasterController.drawingCanvas.on('mouse:move', DM_viewActionsHandler.pan_mousemove);
      MasterController.drawingCanvas.on('mouse:up', DM_viewActionsHandler.pan_mouseup);

      MasterController.drawingCanvas.on('mouse:down', DM_drawingActionsHandler.drawPolygon_mousedown);
      MasterController.drawingCanvas.on('mouse:move', DM_drawingActionsHandler.drawPolygon_mousemove);
      MasterController.drawingCanvas.on('mouse:dblclick', DM_drawingActionsHandler.drawPolygon_mousedblclick);

      MasterController.drawingCanvas.on('mouse:up', DM_drawingActionsHandler.drawTile_mouseup);
      MasterController.drawingCanvas.on('mouse:move', DM_drawingActionsHandler.drawTile_mousemove);

      MasterController.drawingCanvas.on('mouse:up', MasterController._showIDs);
      MasterController.drawingCanvas.on('mouse:down', MasterController._showIDs);


      MasterController.drawingCanvas.on('selection:created', DM_viewActionsHandler.manageGroupingOrder_selectioncreated);
      MasterController.drawingCanvas.on('selection:cleared', DM_viewActionsHandler.manageGroupingOrder_selectioncleared);
      MasterController.drawingCanvas.on('selection:updated', DM_viewActionsHandler.manageGroupingOrder_selectionupdated);


      MasterController.drawingCanvas.on('mouse:over', DM_viewActionsHandler.showInfo_mouseover);
      MasterController.drawingCanvas.on('mouse:out', DM_viewActionsHandler.hideInfo_mouseout);



      MasterController.drawingCanvas.on('object:added', function(e) {
        e.target.meta = e.target.meta ? e.target.meta : {
          id: null,
          order: null,
          group: null,
          spotType: null,
          loadTypes: null
        }
        e.target.uid = MasterController.generateId()
        e.target.stateCache = DM_commandsController.getState(e.target)
      });

      MasterController.drawingCanvas.on('object:modified', function(e) {
        console.log("MODIFIED")
        console.log(e)
        // e.transform.original = JSON.parse(JSON.stringify(e.target))
        DM_commandsController.push(e)
        MasterController.past_event = e
      });



      MasterController.drawingToolbarCanvas = new fabric.Canvas('drawingToolbar');
      MasterController.drawingToolbarCanvas.setHeight(containterHeight);
      MasterController.drawingToolbarCanvas.backgroundColor = Constants.COLORS.TOOLBAR_BACKGROUND;
      MasterController.drawingToolbarCanvas.requestRenderAll();
      DM_controller.drawingToolbarCanvas = MasterController.drawingToolbarCanvas

      DM_toolbarHandler.loadLeftToolbar(this.drawingToolbarCanvas)

    }



  }



}