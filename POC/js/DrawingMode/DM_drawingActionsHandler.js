class DM_drawingActionsHandler {

  static polygonPoints = [];
  static finalPolygon = null;
  static pointers = [];

  static align(axis) {
    var selection = MasterController.canvasInAction.getActiveObject();
    MasterController.canvasInAction.discardActiveObject()
    if ((selection != null) && (selection._objects != undefined)) {
      var master_obj = selection._objects[selection._objects.length - 1];
      var master_coordinate = master_obj[axis];
      if (axis == 'left') {
        selection._objects.forEach(o => {
          o.set({
            left: master_coordinate
          })
        });

      } else if (axis == 'top') {
        selection._objects.forEach(o => {
          o.set({
            top: master_coordinate
          })
        });
      }
      MasterController.canvasInAction.requestRenderAll();

    }
  }

  static snapAngle(degree) {
    var selection = MasterController.canvasInAction.getActiveObject();
    MasterController.canvasInAction.discardActiveObject();
    if (selection != null) {
      if (selection._objects != undefined) {
        selection._objects.forEach(o => {
          o.set({
            angle: degree
          })
        });
      } else {
        selection.set({
          angle: degree
        })
      }

      MasterController.canvasInAction.requestRenderAll();
    }
  }
  static drawPointer(point) {
    var pointer = new fabric.Rect({
      width: 15,
      height: 15,
      left: point.x,
      top: point.y,
      opacity: 1,
      fill: 'black',
      stroke: 'white',
      strokeWidth: 2,
      selectable: false,
      evented: false,
      originX: 'center',
      originY: 'center',
    })
    DM_controller.drawingCanvas.add(pointer);
    pointer.meta = 'ignore'

    return pointer;
  }

  static getPoint(opts) {

    var offset = MasterController.canvasInAction._offset;
    var x = (opts.e.pageX - MasterController.canvasInAction.viewportTransform[4] - offset.left) * (1 / MasterController.canvasInAction.viewportTransform[0]);
    var y = (opts.e.pageY - MasterController.canvasInAction.viewportTransform[5] - offset.top) * (1 / MasterController.canvasInAction.viewportTransform[3]);

    return new Point(x, y);
  }

  static getPolygon(points) {
    return new fabric.Polygon(points, {
      strokeWidth: 3,
      fill: Constants.COLORS.STATIC,
      stroke: 'white',
      selectable: false,
      evented: false,
      opacity: 0.8,
    });
  }

  static getPolygonFrom3Points(Xa, Ya, Xb, Yb, Xc, Yc) {



    var A = Yb - Ya;

    var B = Xa - Xb;

    var C = Xb * Ya - Xa * Yb;

    var C_ = Yc * (Xb - Xa) + Xc * (Ya - Yb);

    var Xa_ = (B * B * Xa - A * (B * Ya + C_)) / (A * A + B * B);

    var Ya_ = (A * A * Ya - B * (A * Xa + C_)) / (A * A + B * B);


    var Xb_ = (B * B * Xb - A * (B * Yb + C_)) / (A * A + B * B);

    var Yb_ = (A * A * Yb - B * (A * Xb + C_)) / (A * A + B * B);



    var ux = (Xa_ - Xb_) / Math.sqrt(Math.pow(Xa_ - Xb_, 2) + Math.pow(Ya_ - Yb_, 2));
    var uy = (Ya_ - Yb_) / Math.sqrt(Math.pow(Xa_ - Xb_, 2) + Math.pow(Ya_ - Yb_, 2));
    var d = Math.sqrt(Math.pow(Xa - Xb, 2) + Math.pow(Ya - Yb, 2))

    var coords = [
      [Xa, Ya],
      [Xc + (d * ux), Yc + (d * uy)],
      [Xc, Yc],
      [Xb, Yb]
    ]
    return coords;
  }

  static getEquidistantPoints(Xs, Ys, Xe, Ye, numPoints) {



    var dx = Xe - Xs;
    var dy = Ye - Ys;



    var result = new Array;

    var stepx = dx / numPoints;
    var stepy = dy / numPoints;
    var px = Xs + stepx;
    var py = Ys + stepy;
    for (let ix = 0; ix < numPoints; ix++) {
      result.push([px, py]);
      px += stepx;
      py += stepy;
    }

    return result;
  }

  static getEdgeLength(edge) {
    return Math.sqrt(Math.pow(edge[0][0] - edge[1][0], 2) + Math.pow(edge[0][1] - edge[1][1], 2))
  }

  static getAngle(edge) {
    return Math.atan((edge[0][0] - edge[1][0]) / (edge[0][1] - edge[1][1]));
  }

  static getSortedKeys(obj) {
    var keys = Object.keys(obj);
    return keys.sort(function(a, b) {
      return obj[b] - obj[a]
    });
  }

  static getLengthWidthEdges(edges) {


    var lengthEdges = new Array()
    var widthEdges = new Array()
    var edges_details = {
      0: DM_drawingActionsHandler.getEdgeLength(edges[0]),
      1: DM_drawingActionsHandler.getEdgeLength(edges[1]),
      2: DM_drawingActionsHandler.getEdgeLength(edges[2]),
      3: DM_drawingActionsHandler.getEdgeLength(edges[3]),
    }
    var sorted_keys = DM_drawingActionsHandler.getSortedKeys(edges_details)


    var lengths = [
      edges[parseInt(sorted_keys[0])],
      edges[parseInt(sorted_keys[1])]
    ]
    var widths = [
      edges[parseInt(sorted_keys[2])],
      edges[parseInt(sorted_keys[3])]
    ]

    return [lengths, widths]

  }

  static drawAreaSegmants(Xa, Ya, Xb, Yb, Xc, Yc, numAreas) {

    var equidistantPointsA = DM_drawingActionsHandler.getEquidistantPoints(Xa, Ya, Xb, Yb, numAreas)

    var [
      [_, _],
      [Xd, Yd],
      [Xc, Yc],
      [_, _]
    ] = DM_drawingActionsHandler.getPolygonFrom3Points(Xa, Ya, Xb, Yb, Xc, Yc);
    var equidistantPointsB = DM_drawingActionsHandler.getEquidistantPoints(Xd, Yd, Xc, Yc, numAreas)



    var [xa, ya] = [Xa, Ya];
    var [xd, yd] = [Xd, Yd];

    for (let i = 0; i < equidistantPointsA.length; i++) {

      var xc = equidistantPointsB[i][0];
      var yc = equidistantPointsB[i][1];
      var xb = equidistantPointsA[i][0];
      var yb = equidistantPointsA[i][1];
      var coords = [
        new Point(xa, ya),
        new Point(xd, yd),
        new Point(xc, yc),
        new Point(xb, yb),
      ];




      var [
        [l1, l2],
        [w1, w2]
      ] = DM_drawingActionsHandler.getLengthWidthEdges([
        [
          [xa, ya],
          [xd, yd]
        ],
        [
          [xd, yd],
          [xc, yc]
        ],
        [
          [xc, yc],
          [xb, yb]
        ],
        [
          [xb, yb],
          [xa, ya]
        ],
      ]);



      var poly = new fabric.Polygon(coords, {
        opacity: 0.8,
        fill: Constants.COLORS.STATIC,
        stroke: 'white',
        strokeWidth: 3,
        originY: 'center',
        originX: 'center',
      });


      var rect = new fabric.Rect({
        top: poly.top,
        left: poly.left,
        width: DM_drawingActionsHandler.getEdgeLength(w1),
        height: DM_drawingActionsHandler.getEdgeLength(l1),
        angle: DM_drawingActionsHandler.getAngle(l1) * (-180 / Math.PI),
        opacity: 0.8,
        fill: Constants.COLORS.STATIC,
        stroke: 'white',
        strokeWidth: 3,
        originY: 'center',
        originX: 'center',
      });

      MasterController.canvasInAction.add(rect);
      rect.meta.interactive = false;
      rect.meta.type = 'static';
      MasterController.canvasInAction.requestRenderAll();

      if (DM_controller.populateOnTheFly == true) {
        MasterController.canvasInAction.setActiveObject(rect)
        DM_toolbarHandler.populateIDs()
      }

      [xa, ya] = [xb, yb];
      [xd, yd] = [xc, yc];

    }



  }




  static drawPolygon_mousemove(opts) {

    if (DM_controller.drawPolygonMode) {
      var new_point = DM_drawingActionsHandler.getPoint(opts);

      if (DM_controller.drawingCanvas._objects.includes(DM_drawingActionsHandler.finalPolygon)) {
        DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.finalPolygon)
      }

      if (DM_drawingActionsHandler.polygonPoints.length >= 1) {
        if (opts.e.ctrlKey === true) {
          new_point.x = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].x
        } else if (opts.e.shiftKey === true) {
          new_point.y = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].y
        }
      }

      DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.pointers[DM_drawingActionsHandler.pointers.length - 1]);
      DM_drawingActionsHandler.pointers.splice(-1);
      DM_drawingActionsHandler.pointers.push(DM_drawingActionsHandler.drawPointer(new_point));

      if (DM_drawingActionsHandler.polygonPoints.length >= 2) {
        DM_drawingActionsHandler.finalPolygon = DM_drawingActionsHandler.getPolygon([DM_drawingActionsHandler.polygonPoints, new_point].flat());
        DM_controller.drawingCanvas.add(DM_drawingActionsHandler.finalPolygon);
        DM_controller.drawingCanvas.requestRenderAll();
      }
    }
  }

  static drawPolygon_mousedblclick(opts) {

    if (DM_controller.drawPolygonMode) {
      DM_controller.drawPolygonMode = false;

      DM_drawingActionsHandler.finalPolygon.set({
        opacity: 0.8,
        fill: Constants.COLORS.STATIC, //'red',
        stroke: 'white',
        strokeWidth: 3,
        selectable: true,
        evented: true,
      })
      DM_drawingActionsHandler.finalPolygon.meta.interactive = false
      DM_drawingActionsHandler.finalPolygon.meta.type = 'static'


      DM_controller.drawingCanvas.setActiveObject(DM_drawingActionsHandler.finalPolygon);

      MasterController.copy();
      DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.finalPolygon);
      MasterController.paste(true);


      // toolbar_canvas.discardActiveObject().requestRenderAll();

      DM_drawingActionsHandler.polygonPoints = [];
      DM_drawingActionsHandler.pointers.forEach(a => {
        DM_controller.drawingCanvas.remove(a);
      });
      DM_drawingActionsHandler.pointers = [];

      MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
    }
  }
  // static showSelectionBox_mousedown(opts){
  //   MasterController.showSelectionBox()
  // }

  static drawPolygon_mousedown(opts) {
    console.log("drawPolygon_mousedown")
    if (DM_controller.drawPolygonMode && opts.e.altKey === false) {
      var new_point = DM_drawingActionsHandler.getPoint(opts);

      if (DM_drawingActionsHandler.polygonPoints.length >= 1) {
        if (opts.e.ctrlKey === true) {
          new_point.x = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].x
        } else if (opts.e.shiftKey === true) {
          new_point.y = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].y
        }

      }

      DM_drawingActionsHandler.polygonPoints.push(new_point);
      DM_drawingActionsHandler.pointers.push(DM_drawingActionsHandler.drawPointer(new_point));
    }
  }



  static drawTile_mousemove(opts) {

    if (DM_controller.drawTileMode) {
      var new_point = DM_drawingActionsHandler.getPoint(opts);

      if (DM_controller.drawingCanvas._objects.includes(DM_drawingActionsHandler.finalPolygon)) {
        DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.finalPolygon)
      }

      if (DM_drawingActionsHandler.polygonPoints.length >= 1) {
        if (opts.e.ctrlKey === true) {
          new_point.x = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].x
        } else if (opts.e.shiftKey === true) {
          new_point.y = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].y
        }
      }

      DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.pointers[DM_drawingActionsHandler.pointers.length - 1]);
      DM_drawingActionsHandler.pointers.splice(-1);
      DM_drawingActionsHandler.pointers.push(DM_drawingActionsHandler.drawPointer(new_point));

      if (DM_drawingActionsHandler.polygonPoints.length >= 2) {
        DM_drawingActionsHandler.finalPolygon = DM_drawingActionsHandler.getPolygon([DM_drawingActionsHandler.polygonPoints, new_point].flat());
        DM_controller.drawingCanvas.add(DM_drawingActionsHandler.finalPolygon);
        DM_controller.drawingCanvas.requestRenderAll();
      }
    }
  }

  static drawTile_mouseup(opts) {
    console.log("drawTile_mouseup")
    if (DM_controller.drawTileMode && opts.e.altKey === false) {
      var new_point = DM_drawingActionsHandler.getPoint(opts);

      if (DM_drawingActionsHandler.polygonPoints.length >= 1) {
        if (opts.e.ctrlKey === true) {
          new_point.x = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].x
        } else if (opts.e.shiftKey === true) {
          new_point.y = DM_drawingActionsHandler.polygonPoints[DM_drawingActionsHandler.polygonPoints.length - 1].y
        }

      }

      DM_drawingActionsHandler.polygonPoints.push(new_point);
      DM_drawingActionsHandler.pointers.push(DM_drawingActionsHandler.drawPointer(new_point));
    }

    if (DM_drawingActionsHandler.polygonPoints.length == 3 && DM_controller.drawTileMode) {
      var n = parseInt(prompt("How many areas?"))

      DM_drawingActionsHandler.drawAreaSegmants(
        DM_drawingActionsHandler.polygonPoints[0].x, DM_drawingActionsHandler.polygonPoints[0].y,
        DM_drawingActionsHandler.polygonPoints[1].x, DM_drawingActionsHandler.polygonPoints[1].y,
        DM_drawingActionsHandler.polygonPoints[2].x, DM_drawingActionsHandler.polygonPoints[2].y,
        n
        // parseInt(prompt("How many areas?"))
      );

      DM_drawingActionsHandler.polygonPoints = [];
      DM_drawingActionsHandler.pointers.forEach(a => {
        DM_controller.drawingCanvas.remove(a);
      });
      DM_drawingActionsHandler.pointers = [];
      // toolbar_canvas.discardActiveObject().requestRenderAll();
      DM_controller.drawPolygonMode = false;
      DM_controller.drawTileMode = false;
      DM_controller.drawingCanvas.remove(DM_drawingActionsHandler.finalPolygon)
      DM_drawingActionsHandler.finalPolygon = null;
      // canvasChangeCursor("default")
      MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
      // MasterController.showSelectionBox()
    }
  }



}