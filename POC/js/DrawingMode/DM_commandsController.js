class DM_commandsController {
  static undoStack = [];
  static redoStack = [];

  static trackedAttributes = ['angle', 'flipX', 'flipY', 'originX', 'originY', 'scaleX', 'scaleY', 'skewX', 'skewY', 'top', 'left']

  static getState(object) {
    return DM_commandsController.trackedAttributes.reduce((prev, curr) => {
      // console.log(curr)
      prev[curr] = object[curr];
      if (curr.includes("originX")) {
        prev[curr] = 'left'
        // console.log("changed " + curr + "  from " + object[curr] + " to " + prev[curr])
      }
      if (curr.includes("originY")) {
        prev[curr] = 'top'
        // console.log("changed " + curr + "  from " + object[curr] + " to " + prev[curr])
      }
      return prev
    }, {})
  }

  // static isEqual(stateA, stateB) {
  //   return Object.keys(stateA).reduce((prev, curr) => {
  //     var _ = stateA[curr] == stateB[curr];
  //     return _ && prev
  //   }, true)
  // }

  static push(event) {

    MasterController.canvasInAction.discardActiveObject().requestRenderAll();
    //clear redo stack
    DM_commandsController.redoStack = []
    if (event.target._objects == null) {
      DM_commandsController.undoStack.push({
        'uid': event.transform.target.uid,
        'original_state': DM_commandsController.getState(event.transform.original),
        'new_state': DM_commandsController.getState(event.transform.target)
      })
    } else {
      event.target._objects.forEach((item, i) => {
        DM_commandsController.undoStack.push({
          'uid': item.uid,
          'original_state': item.stateCache,
          'new_state': DM_commandsController.getState(item)
        });
        item.stateCache = DM_commandsController.getState(item)
      });

    }
  }


  static undo() {
    if (DM_commandsController.undoStack.length > 0) {
      var transform_info = DM_commandsController.undoStack.pop()
      // var targetObject = MasterController.canvasInAction._objects.filter(o => DM_commandsController.isEqual(DM_commandsController.getState(o), transform_info.new_state))[0]
      var targetObject = MasterController.canvasInAction._objects.filter(o => o.uid == transform_info.uid)[0]

      targetObject.set(transform_info.original_state)
      MasterController.canvasInAction.requestRenderAll()

      var original_state = MasterController.deepCopy(transform_info.original_state)
      transform_info.original_state = MasterController.deepCopy(transform_info.new_state)
      transform_info.new_state = MasterController.deepCopy(original_state)

      DM_commandsController.redoStack.push(transform_info)
      MasterController._showIDs();
    }
  }

  static redo() {
    if (DM_commandsController.redoStack.length > 0) {

      var transform_info = DM_commandsController.redoStack.pop()
      // var targetObject = MasterController.canvasInAction._objects.filter(o => DM_commandsController.isEqual(DM_commandsController.getState(o), transform_info.new_state))[0]
      var targetObject = MasterController.canvasInAction._objects.filter(o => o.uid == transform_info.uid)[0]

      targetObject.set(transform_info.original_state)
      MasterController.canvasInAction.requestRenderAll()
      targetObject.stateCache = DM_commandsController.getState(targetObject)


      var original_state = MasterController.deepCopy(transform_info.original_state)
      transform_info.original_state = MasterController.deepCopy(transform_info.new_state)
      transform_info.new_state = MasterController.deepCopy(original_state)

      DM_commandsController.undoStack.push(transform_info)
      MasterController._showIDs();
    }
  }



}