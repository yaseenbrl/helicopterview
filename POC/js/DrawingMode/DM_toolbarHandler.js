class DM_toolbarHandler {

  static tools = [];

  static drawPolygonOn() {
    console.log("running  drawPolygonOn");
    DM_controller.drawPolygonMode = true;
    DM_controller.drawTileMode = false;
    // canvasChangeCursor("crosshair");

  }

  static drawTile() {
    console.log("running  drawTile");
    DM_controller.drawPolygonMode = false;
    DM_controller.drawTileMode = true;

    // canvasChangeCursor("crosshair");
    console.log("Draw polygon");
  }

  static addTool(image, toolBar, selectionHandler, order, backgroundOn = true, name = "test") {
    console.log("Adding tool: " + name)



    fabric.Image.fromURL(image, function(oImg) {
      var rect = new fabric.Rect({
        selectable: false,
        evented: false
      })
      oImg.set({
        left: toolBar.width / 2 - 16,
        top: (40 * order) - 15,
        width: 32,
        height: 32,
        angle: 0,
        opacity: 1,
        selectable: true,
        stroke: 'rgba(0,0,0,0)',
        strokeWidth: 2,
        borderColor: '#464646',
        borderScaleFactor: 1,
        lockMovementX: true,
        lockMovementY: true,
        hasControls: false,
        bgRect: rect,
      })
      rect.set({
        width: oImg.width * 1.5,
        height: oImg.height * 1.5,
        top: oImg.getCenterPoint().y,
        left: oImg.getCenterPoint().x,
        fill: name !== 'Assign Off' ? Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE : '#eee',
        rx: 10,
        ry: 10,
        originX: 'center',
        originY: 'center',
        opacity: backgroundOn ? 1 : 0,
      })
      var textObj = new fabric.Text(name, {
        fill: Constants.COLORS.TOOLBAR_TEXT_COLOR,
        fontSize: 13,
        left: rect.left,
        top: rect.top + rect.height / 2 + 10,
        originX: 'center',
        originY: 'center',
        selectable: false,
        evented: false,
        _id: name,
        fontFamily: Constants.FONT_FAMILY,
      })
      console.log("ADDED " + name)

      toolBar.add(rect);
      toolBar.add(oImg);
      toolBar.add(textObj);


      oImg.on('selected', function() {
        selectionHandler();
        MasterController.colorObjects();
        var activeObject = toolBar.getActiveObject()
        if (activeObject) {
          // activeObject.set({
          //   stroke: '#f00',
          //   strokeWidth: 2,
          // })

          oImg.bgRect.set({
            fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_ACTIVE
          })

        }
      });

      oImg.on('deselected', function() {
        // oImg.set({
        //   stroke: 'rgba(0,0,0,0)',
        //   strokeWidth: 2,
        // })

        oImg.bgRect.set({
          fill: name !== 'Assign Off' ? Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE : '#eee'
        })
      });

      oImg.on('mouseover', function() {
        // oImg.animate('scaleX', 1.05, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        // oImg.animate('scaleY', 1.05, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        oImg.bgRect.set({
          fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_ACTIVE
        })
        toolBar.requestRenderAll();
      });
      oImg.on('mouseout', function() {
        // oImg.animate('scaleX', 1.0, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        // oImg.animate('scaleY', 1.0, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        if (toolBar.getActiveObject() !== oImg) {
          oImg.bgRect.set({
            fill: name !== 'Assign Off' ? Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE : '#eee'
          })
          toolBar.requestRenderAll();
        }

      });

      //dirty if statement to accomodate new requirements
      if (name == 'Assign Off') {
        try {
          toolBar.remove(toolBar._objects.filter(o => o._id && o._id === 'Assign On')[0])
        } catch (e) {
          console.log(e)
        }

      } else if (name == 'Assign On') {
        try {
          toolBar.remove(toolBar._objects.filter(o => o._id && o._id === 'Assign Off')[0])
        } catch (e) {
          console.log(e)
        }
      }

      DM_toolbarHandler.tools.push(oImg);
    });

    toolBar.requestRenderAll();
  }

  static setCanvasBackground() {
    console.log("Setting background")

    var imgInput = document.createElement('input');
    imgInput.type = "file";

    imgInput.onsubmit = function(e) {
      MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
    }

    imgInput.onchange = function(e) {

      var url = URL.createObjectURL(e.target.files[0]);
      fabric.Image.fromURL(url, function(img) {
        var oImg = img.set({
          left: 0,
          top: 0,
          selectable: false,
          evented: false,
        })

        // normalize size
        oImg.set({
          scaleX: 2 * Math.min(MasterController.drawingCanvas.width / oImg.width, MasterController.drawingCanvas.height / oImg.height),
          scaleY: 2 * Math.min(MasterController.drawingCanvas.width / oImg.width, MasterController.drawingCanvas.height / oImg.height)
        })

        // delete current background, if it exists [removes all canvas objects of type 'image' ]
        for (var i = 0; i < MasterController.drawingCanvas._objects.length; i++) {
          if (MasterController.drawingCanvas._objects[i].type === 'image') {
            MasterController.drawingCanvas.remove(MasterController.drawingCanvas._objects[i])
          }
        }

        MasterController.drawingCanvas.add(oImg)
        MasterController.drawingCanvas.sendToBack(oImg)
        MasterController.drawingCanvas.requestRenderAll()


        MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();

      });


    }

    $(imgInput).click();

  }

  static getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

  static sortObjectByValues(unordered) {
    return Object.values(unordered).sort().reduce(
      (obj, value) => {
        obj[DM_toolbarHandler.getKeyByValue(unordered, value)] = unordered[DM_toolbarHandler.getKeyByValue(unordered, value)];
        return obj;
      }, {}
    );
  }

  static upload() {

    var _json = MasterController.canvasInAction.toJSON("meta")
    _json["objects"] = _json.objects.filter(o => o.type != "image" & o.type != "text")

    _json["viewportTransform"] = MasterController.canvasInAction.viewportTransform.slice()

    MasterController.saveToFile(JSON.stringify(_json), "json", "yard")
    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
  }

  static download() {

    MasterController.getFileContent(_ => {
      MasterController.canvasInAction.loadFromJSON(JSON.parse(_));
      if ((MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.groupNumber)).length > 0) {
        DM_controller.groupsCounter = 1 + Math.max(...MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.groupNumber).map(o => parseInt(o.meta.groupNumber)))

      } else {
        DM_controller.groupsCounter = 0

      }
      MasterController._showIDs()
      MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
      MasterController.colorObjects();
    })


  }

  static switchPopulateOnTheFly() {
    //flipping the icon
    MasterController.drawingToolbarCanvas.remove(DM_toolbarHandler.tools[DM_toolbarHandler.tools.length - 1]);
    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
    if (DM_controller.populateOnTheFly) {
      DM_toolbarHandler.addTool('./assets/icons/switch-off.png', MasterController.drawingToolbarCanvas, DM_toolbarHandler.switchPopulateOnTheFly, 13, true, 'Assign Off')
    } else {
      DM_toolbarHandler.addTool('./assets/icons/switch-on.png', MasterController.drawingToolbarCanvas, DM_toolbarHandler.switchPopulateOnTheFly, 13, true, 'Assign On')
    }
    DM_controller.populateOnTheFly = !DM_controller.populateOnTheFly
  }

  static populateIDs() {
    var canvas = MasterController.canvasInAction
    var activeObject = canvas.getActiveObject();



    if (activeObject.type === "activeSelection") {
      var objects = activeObject._objects;

      var start_id = prompt("What's the first ID?");
      // var end_id = prompt("What's the last ID?");

      var IDs = MasterController.generateIDs(start_id, objects.length);
      if (IDs === null || IDs.length !== objects.length) {
        alert("Generated IDs number doesn't match the number of objects selected")
      } else {
        for (var i = 0; i < objects.length; i++) {
          objects[i].meta.id = IDs[i]
          objects[i].meta.interactive = true
          objects[i].meta.type = 'spot'
          objects[i].fill = 'green';

        }
      }


    } else {

      activeObject.meta.id = prompt("Insert the object's ID")
      activeObject.meta.interactive = true
      activeObject.meta.type = 'spot'
      activeObject.set('fill', 'green');

    }

    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
    canvas.discardActiveObject();
    canvas.requestRenderAll();
    MasterController._showIDs();
  }


  static addSection() {
    var canvas = MasterController.canvasInAction
    var activeObject = canvas.getActiveObject();
    var n = 0; //tracking the number of areas added

    if (activeObject.type === "activeSelection") {
      var objects = activeObject._objects;

      var section = prompt("Which section would you like to add those yard areas to?");

      if (section !== null)
        for (var i = 0; i < objects.length; i++) {
          objects[i].meta.section = section
          n += 1
        }

    } else {
      var section = prompt("Which section would you like to add this yard area to?")
      if (section !== null)
        activeObject.meta.section = section
      n += 1

    }

    if (section !== null)
      alert("Assigned " + (n).toString() + " new area(s) to section: " + section)

    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
    canvas.discardActiveObject();
    canvas.requestRenderAll();
    MasterController._showIDs();
  }

  static groubObjects() {
    if (DM_controller._orderedSelection) {
      MasterController.canvasInAction.getActiveObject()._objects.forEach((item, i) => {
        item.meta.order = DM_controller.selectionOrder[item.meta.id].toString()
        item.meta.groupNumber = DM_controller.groupsCounter.toString()
        item.fill = Constants.COLORS.groupedSpot
        item.stroke = item.meta.order === '1' ? 'red' : 'white'
        item.strokeWidth = item.meta.order === '1' ? 3 : 1
      });
      DM_controller.groupsCounter += 1
      MasterController.canvasInAction.discardActiveObject()
      MasterController.canvasInAction.requestRenderAll()
    }
    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
  }

  static assignCapabilities() {
    var canvas = MasterController.canvasInAction
    var activeObject = canvas.getActiveObject();

    var loadTypesDOM = document.getElementsByClassName("loadTypes");
    var selectedLoadTypes = []
    for (let item of loadTypesDOM) {
      console.log(item.value + ">>" + item.checked);
      if (item.checked) {
        selectedLoadTypes.push(item.value)
      }
    }

    if (activeObject.type === "activeSelection") {
      var objects = activeObject._objects;

      for (var i = 0; i < objects.length; i++) {

        objects[i].meta.loadTypes = selectedLoadTypes
        objects[i].meta.spotType = document.querySelector('input[name="spotType"]:checked').value;

      }



    } else {
      activeObject.meta.loadTypes = selectedLoadTypes
      activeObject.meta.spotType = document.querySelector('input[name="spotType"]:checked').value;



    }

    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();
    canvas.discardActiveObject().requestRenderAll();

  }

  static exportToTable() {
    if (MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id).length > 0) {
      MasterController.exportToTable()
    } else {
      alert("No exportable data could be processed")
    }
    MasterController.drawingToolbarCanvas.discardActiveObject().requestRenderAll();


  }

  static loadLeftToolbar(toolbar_canvas) {
    console.log("running  loadLeftToolbar");

    toolbar_canvas.backgroundColor = Constants.COLORS.TOOLBAR_BACKGROUND;

    DM_toolbarHandler.addTool('./assets/icons/draw_tile.png', toolbar_canvas, DM_toolbarHandler.drawTile, 1, true, "Draw")
    DM_toolbarHandler.addTool('./assets/icons/draw_polygon.png', toolbar_canvas, DM_toolbarHandler.drawPolygonOn, 3, true, "Draw Static")
    DM_toolbarHandler.addTool('./assets/icons/export-yard.png', toolbar_canvas, DM_toolbarHandler.upload, 5, true, "Export") //4
    DM_toolbarHandler.addTool('./assets/icons/import-yard.png', toolbar_canvas, DM_toolbarHandler.download, 7, true, "Import") //5
    DM_toolbarHandler.addTool('./assets/icons/blueprint.png', toolbar_canvas, DM_toolbarHandler.setCanvasBackground, 9, true, "Upload") //7
    DM_toolbarHandler.addTool('./assets/icons/hash.png', toolbar_canvas, DM_toolbarHandler.populateIDs, 11, true, "Assign id") //9
    DM_toolbarHandler.addTool('./assets/icons/group.png', toolbar_canvas, DM_toolbarHandler.groubObjects, 15, true, "Group") //15
    DM_toolbarHandler.addTool('./assets/icons/assign.png', toolbar_canvas, DM_toolbarHandler.assignCapabilities, 17, true, "Edit") //16

    DM_toolbarHandler.addTool('./assets/icons/section.png', toolbar_canvas, DM_toolbarHandler.addSection, 19, true, "Section") //21

    DM_toolbarHandler.addTool('./assets/icons/exportToTable.png', toolbar_canvas, DM_toolbarHandler.exportToTable, 21, true, "Report") //19




    DM_toolbarHandler.addTool('./assets/icons/switch-off.png', toolbar_canvas, DM_toolbarHandler.switchPopulateOnTheFly, 13, true, 'Assign Off') //10

    toolbar_canvas.requestRenderAll()
  }

}