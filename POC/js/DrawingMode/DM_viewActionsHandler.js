class DM_viewActionsHandler {


  static zoomDelta = 1;
  static zoomAcc = 0;


  static zoom_wheel(opts) {


    if (opts.e.altKey === true) {
      var delta = opts.e.deltaY;
      // var new_point = DM_drawingActionsHandler.getPoint(opts);
      DM_viewActionsHandler.zoomAcc += delta
      // var zoom = this.getZoom();
      // zoom *= 0.999 ** delta;
      // if (zoom > 20) zoom = 20;
      // if (zoom < 0.01) zoom = 0.01;
      // console.log('zoom : ' +zoom)
      if (Math.abs(DM_viewActionsHandler.zoomAcc) > DM_viewActionsHandler.zoomDelta) {

        var zoom = this.getZoom();
        zoom *= 0.999 ** DM_viewActionsHandler.zoomAcc;
        if (zoom > 20) zoom = 20;
        if (zoom < 0.01) zoom = 0.01;

        DM_viewActionsHandler.zoomAcc = 0
        // this.setZoom(zoom);
        // MasterController.canvasInAction.zoomToPoint(new_point, zoom)
        MasterController.canvasInAction.zoomToPoint({
          x: opts.e.offsetX,
          y: opts.e.offsetY
        }, zoom);

      }


      // this.setZoom(zoom);

      opts.e.preventDefault();
      opts.e.stopPropagation();
    }
    // this.requestRenderAll()
  }

  static pan_mousedown(opts) {
    var evt = opts.e;
    if (evt.altKey === true) {
      this.isDragging = true;
      this.selection = false;
      this.lastPosX = evt.clientX;
      this.lastPosY = evt.clientY;
    }
  }

  static pan_mousemove(opts) {

    if (this.isDragging) {
      var e = opts.e;
      var vpt = this.viewportTransform;
      vpt[4] += e.clientX - this.lastPosX;
      vpt[5] += e.clientY - this.lastPosY;
      this.lastPosX = e.clientX;
      this.lastPosY = e.clientY;
      this.requestRenderAll();
    }
  }

  static pan_mouseup(opts) {
    this.setViewportTransform(this.viewportTransform);
    this.isDragging = false;
    this.selection = true;
  }

  static manageGroupingOrder_selectioncreated(opts) {
    if (!opts.target._objects) { // if one object has been selected
      DM_controller._currentSelectionOrder = 1;
      DM_controller._orderedSelection = true
      DM_controller._oldSelection = [opts.target]
      // opts.target.meta.group = DM_controller.groupsCounter
      // opts.target.meta.order = DM_controller.selectionOrder
      DM_controller.selectionOrder[opts.target.meta.id] = DM_controller._currentSelectionOrder
    }
  }
  static manageGroupingOrder_selectioncleared(opts) {
    DM_controller._currentSelectionOrder = 1;
    DM_controller._oldSelection = null;
    DM_controller.selectionOrder = {}
    DM_controller._orderedSelection = false
    MasterController.setInfoBar("")
  }
  static manageGroupingOrder_selectionupdated(opts) {

    if (!MasterController.canvasInAction.getActiveObject()._objects) {
      DM_viewActionsHandler.manageGroupingOrder_selectioncleared(opts)
      DM_viewActionsHandler.manageGroupingOrder_selectioncreated(opts)
    }

    if (DM_controller._orderedSelection && opts.target._objects) {

      //handle Removed Objects

      var removed_objects = DM_controller._oldSelection.filter(o => !opts.target._objects.includes(o));
      if (removed_objects.length == 1) {
        var _decrement = false
        for (var key in DM_controller.selectionOrder) {
          if (key == removed_objects[0].meta.id) {
            _decrement = true
          }
          if (_decrement == true) {
            DM_controller.selectionOrder[key] -= 1
          }
        }
        DM_controller._currentSelectionOrder -= 1
      }

      removed_objects.map(o => o.meta.id).map(id => {
        delete DM_controller.selectionOrder[id];
      })
      removed_objects.map(o => DM_controller._oldSelection.splice(DM_controller._oldSelection.indexOf(o), 1))

      //handle Added Objects

      var added_objects = opts.target._objects.filter(o => !DM_controller._oldSelection.includes(o));
      // console.log("New to the selection:" + added_objects.map(o => o.meta.id))
      added_objects.forEach((item, i) => {
        DM_controller._currentSelectionOrder += 1
        // console.log("assigned order [" + DM_controller._currentSelectionOrder + "]  to id: " + item.meta.id)
        // item.meta.order = DM_controller.selectionOrder
        // item.meta.group = DM_controller.groupsCounter
        DM_controller.selectionOrder[item.meta.id] = DM_controller._currentSelectionOrder
        DM_controller._oldSelection.push(item)
      });
    }

    if (MasterController.canvasInAction.getActiveObject()._objects && MasterController.canvasInAction.getActiveObject()._objects.length > 1) {
      var _txt = "Group #" + DM_controller.groupsCounter + " : "
      for (var i = 1; i <= Object.keys(DM_controller.selectionOrder).length; i++) {
        for (var key in DM_controller.selectionOrder) {
          if (DM_controller.selectionOrder[key] == i) {
            _txt += key + '  &rarr;  '
          }

        }
      }
      DM_viewActionsHandler.showInfoBox(_txt.slice(0, _txt.length - 10))

      // MasterController.setInfoBar(_txt.slice(0, _txt.length - 10))
    }

  }

  static showInfo_mouseover(opts) {
    if (opts.target && opts.target.meta && opts.target.meta.id && !DM_controller._orderedSelection) {
      var _html = ""
      for (var key of Object.keys(opts.target.meta)) {
        if (opts.target.meta[key]) {
          if (key === 'type' || key === 'interactive') {
            continue
          }
          _html += "<strong> " + key + " </strong>: " + opts.target.meta[key] + "&nbsp ".repeat(2) + "<br><br>"
        }
      }
      DM_viewActionsHandler.showInfoBox(_html)
    }
  }

  static hideInfo_mouseout(opts) {
    if (opts.target && opts.target.meta && !DM_controller._orderedSelection) {
      DM_viewActionsHandler.hideInfoBox()
    }
  }

  static hideInfoBox() {
    var infoboxContainer = document.getElementsByClassName("infoBox")[0];
    var _html = ""
    infoboxContainer.innerHTML = _html
  }

  static showInfoBox(_html) {
    var infoboxContainer = document.getElementsByClassName("infoBox")[0];
    infoboxContainer.innerHTML = _html
  }

}