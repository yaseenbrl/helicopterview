class IM_viewActionsHandler {

  static showInfoBox(_html) {
    var infoboxContainer = document.getElementsByClassName("infoBox")[1];
    infoboxContainer.innerHTML = _html
  }
  static hideInfoBox() {
    var infoboxContainer = document.getElementsByClassName("infoBox")[1];
    var _html = ""
    infoboxContainer.innerHTML = _html
  }
  static searchBox_handler() {
    var search_box = document.getElementById("search_box")
    var search_text = search_box.value.toLocaleLowerCase()


    if (search_text.length > 0) {
      var highlited = MasterController.canvasInAction._objects.filter(o => o.info && IM_viewActionsHandler._getTextDescription(o).includes(search_text))
      var unhighlited = MasterController.canvasInAction._objects.filter(o => o.info && !IM_viewActionsHandler._getTextDescription(o).includes(search_text))

      IM_viewActionsHandler.unfilterSpots()

      unhighlited.forEach(o => {
        o.objectCaching = false
        o.old = {}
        o.old.fill = o.fill
        o.old.evented = o.evented
        o.old.selectable = o.selectable
        o.old.stroke = o.stroke
        o.set('fill', 'grey')
        o.set('evented', false)
        o.set('selectable', false)
        o.set('stroke', 'white')
      })
      highlited.forEach(o => {
        o.objectCaching = false
        o.old = {}
        o.old.fill = o.fill
        o.old.evented = o.evented
        o.old.stroke = o.stroke
        o.set('fill', 'yellow')
        o.set('evented', true)
      })
      MasterController.canvasInAction.requestRenderAll()

    } else {

      IM_viewActionsHandler.unfilterSpots()

    }




  }


  static _getTextDescription(object) {
    var txt = ""
    if (object.meta) {
      txt += JSON.stringify(object.meta)
    }
    if (object.info) {
      txt += JSON.stringify(object.info)
    }
    return txt.toLocaleLowerCase()
  }

  static zoom_wheel(opts) {
    var delta = opts.e.deltaY;

    if (opts.e.altKey === true) {
      var zoom = this.getZoom();
      zoom *= 0.999 ** delta;
      if (zoom > 20) zoom = 20;
      if (zoom < 0.01) zoom = 0.01;
      // this.setZoom(zoom);
      MasterController.canvasInAction.zoomToPoint({
        x: opts.e.offsetX,
        y: opts.e.offsetY
      }, zoom);

    } else {
      if (IM_controller.dynamicZoomOn) {
        var zoom = IM_controller.dynamicZoomCanvas.getZoom();
        zoom *= 0.999 ** delta;
        if (zoom > 20) zoom = 20;
        if (zoom < 0.01) zoom = 0.01;
        IM_controller.dynamicZoomCanvas.setZoom(zoom);
      }

    }

    if (IM_controller.dynamicZoomOn) {
      IM_viewActionsHandler.renderViewRectangle();
      IM_viewActionsHandler.refreshDynamicZoomCanvas(opts, false);
    }


    this.requestRenderAll()
    opts.e.preventDefault();
    opts.e.stopPropagation();
  }

  static pan_mousedown(opts) {
    var evt = opts.e;
    if (evt.altKey === true) {
      this.isDragging = true;
      this.selection = false;
      this.lastPosX = evt.clientX;
      this.lastPosY = evt.clientY;
    }
  }

  static pan_mousemove(opts) {

    if (this.isDragging) {
      var e = opts.e;
      var vpt = this.viewportTransform;
      vpt[4] += e.clientX - this.lastPosX;
      vpt[5] += e.clientY - this.lastPosY;
      this.lastPosX = e.clientX;
      this.lastPosY = e.clientY;
      this.requestRenderAll();
    }
  }

  static pan_mouseup(opts) {
    this.setViewportTransform(this.viewportTransform);
    this.isDragging = false;
    this.selection = true;
  }

  static async renderViewRectangle() {
    IM_controller.viewRectangle.set({
      width: IM_controller.dynamicZoomCanvas.width / IM_controller.dynamicZoomCanvas.getZoom(),
      height: IM_controller.dynamicZoomCanvas.height / IM_controller.dynamicZoomCanvas.getZoom(),
      left: IM_controller.dynamicZoomCanvas.viewportTransform[4] / -IM_controller.dynamicZoomCanvas.getZoom(),
      top: IM_controller.dynamicZoomCanvas.viewportTransform[5] / -IM_controller.dynamicZoomCanvas.getZoom(),
      strokeWidth: IM_controller.viewRectangle.original_strokeWidth / MasterController.interactiveCanvas.getZoom(),
    })

    MasterController.interactiveCanvas.bringToFront(IM_controller.viewRectangle)
    MasterController.interactiveCanvas.requestRenderAll()

  }

  static refreshDynamicZoomCanvas(opts, reload = true) {


    var vptZ = IM_controller.dynamicZoomCanvas.viewportTransform;
    var canvasZ = IM_controller.dynamicZoomCanvas

    if (reload) {
      MasterController.interactiveCanvas.requestRenderAll()
      IM_controller.dynamicZoomCanvas.loadFromJSON(MasterController.interactiveCanvas.toJSON("meta"));
      IM_controller.dynamicZoomCanvas.remove(IM_controller.dynamicZoomCanvas.item(IM_controller.dynamicZoomCanvas._objects.length - 1))
      IM_controller.dynamicPointer = new fabric.Rect({
        top: (canvasZ.getCenter().top - vptZ[5]) / vptZ[0],
        left: (canvasZ.getCenter().left - vptZ[4]) / vptZ[0],
        width: 5 / vptZ[0],
        height: 5 / vptZ[0],
        strokeWidth: 5 / vptZ[0]
      });
      // IM_controller.dynamicZoomCanvas.add(IM_controller.dynamicPointer)


      IM_controller.dynamicZoomCanvas.requestRenderAll();
    }


    if (opts) {

      var vptO = MasterController.interactiveCanvas.viewportTransform;
      vptZ[4] = -(opts.e.clientX - MasterController.interactiveCanvas._offset.left) * vptZ[0] / vptO[0] + 0.5 * IM_controller.dynamicZoomCanvas.width + vptO[4] * vptZ[0] / vptO[0];
      vptZ[5] = -window.scrollY - (opts.e.clientY - MasterController.interactiveCanvas._offset.top) * vptZ[0] / vptO[0] + 0.5 * IM_controller.dynamicZoomCanvas.height + vptO[5] * vptZ[0] / vptO[0];

    }

    if (IM_controller.dynamicPointer) {
      IM_controller.dynamicPointer.set({
        top: (canvasZ.getCenter().top - vptZ[5]) / vptZ[0],
        left: (canvasZ.getCenter().left - vptZ[4]) / vptZ[0],
        width: 5 / vptZ[0],
        height: 5 / vptZ[0],
        strokeWidth: 5 / vptZ[0],
        objectCaching: false,
      });
      canvasZ.bringToFront(IM_controller.dynamicPointer);

    }

    IM_controller.dynamicZoomCanvas.requestRenderAll();

  }

  static updateViewRectangle_mousemove(opts) {

    if (IM_controller.dynamicZoomOn) {
      IM_viewActionsHandler.refreshDynamicZoomCanvas(opts, false);
      IM_viewActionsHandler.renderViewRectangle();
    }
  }

  static stroke_mouseover(opts) {

    if (opts.target && opts.target.evented) {
      opts.target.set('stroke', Constants.ACTIVE_STROKE_COLOR);
      opts.target.set('strokeWidth', Constants.ACTIVE_STROKE_WIDTH);


      var zObj = IM_controller.dynamicZoomCanvas._objects.filter(o => o.meta && o.meta.id === opts.target.meta.id)[0]
      // console.log("HERE:" )
      // console.log(zObj)
      zObj.set('stroke', Constants.ACTIVE_STROKE_COLOR);
      zObj.set('strokeWidth', Constants.ACTIVE_STROKE_WIDTH);


      IM_controller.dynamicZoomCanvas.requestRenderAll();
      MasterController.interactiveCanvas.requestRenderAll();


      if (IM_controller.MultiZoomActive) {
        MasterController.postMessage({
          'type': 'stroke_mouseover',
          'data': {
            id: opts.target.meta.id
          }
        })
      }


    }




  }

  static unstroke_mouseout(opts) {

    if (opts.target && opts.target.evented) {
      opts.target.set('stroke', Constants.INACTIVE_STROKE_COLOR);
      opts.target.set('strokeWidth', Constants.INACTIVE_STROKE_WIDTH);

      var zObj = IM_controller.dynamicZoomCanvas._objects.filter(o => o.meta && o.meta.id === opts.target.meta.id)[0]
      zObj.set('stroke', Constants.INACTIVE_STROKE_COLOR);
      zObj.set('strokeWidth', Constants.INACTIVE_STROKE_WIDTH);


      IM_controller.dynamicZoomCanvas.requestRenderAll();
      MasterController.interactiveCanvas.requestRenderAll();

      if (IM_controller.MultiZoomActive) {
        MasterController.postMessage({
          'type': 'unstroke_mouseout',
          'data': {
            id: opts.target.meta.id
          }
        })
      }

    }


  }

  static showInfo_mouseover(opts) {

    if (opts.target && opts.target.evented) {
      var infoboxContainer = document.getElementsByClassName("infoBox")[1];
      infoboxContainer.style.visibility = 'visible'
      if (opts.target.meta) {
        var html = ""
        html += opts.target.meta.id ? "<p> <strong>id:</strong>  " + opts.target.meta.id + "</p> " : ""
        html += opts.target.info ? "<p> <strong>status: </strong> " + opts.target.info.status + "</p> " : ""
        if (opts.target.info) {
          html += opts.target.info.unit ? "<p> <strong>unit number:</strong>  " + opts.target.info.unit.num + "</p> " : ""
          html += opts.target.info.unit ? "<p> <strong>unit status:</strong>  " + opts.target.info.unit.status + "</p> " : ""
        }

        infoboxContainer.innerHTML = html
      } else {
        infoboxContainer.innerHTML = '<p>no meta data</p>'
      }
    }


  }

  static hideInfo_mouseout(opts) {

    if (opts.target && opts.target.evented) {
      var infoboxContainer = document.getElementsByClassName("infoBox")[1];
      infoboxContainer.style.visibility = 'hidden'
    }


  }

  static broadcastCanvas_mouseup(opts) {
    if (opts.e.altKey === false) {
      MasterController.postMessage({
        'type': 'canvasJSON',
        'data': MasterController.canvasInAction.toJSON("meta")
      })
    }
  }

  static unfilterSpots() {
    MasterController.canvasInAction._objects.map(o => {
      o.fill = o.old ? o.old.fill : o.fill
      o.evented = o.old ? o.old.evented : o.evented
      o.stroke = o.old ? o.old.stroke : o.stroke
    })
  }

  static filterSpots(status) {
    var highlited = MasterController.canvasInAction._objects.filter(o => o.info && o.info.status === status)
    var unhighlited = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.spotType && (!(o.info) || (o.info.status !== status)))


    unhighlited.forEach(o => {
      o.objectCaching = false
      o.old = {}
      o.old.fill = o.fill
      o.old.evented = o.evented
      o.old.selectable = o.selectable
      o.old.stroke = o.stroke
      o.set('fill', 'grey')
      o.set('evented', false)
      o.set('selectable', false)
      o.set('stroke', 'white')
    })
    highlited.forEach(o => {
      o.objectCaching = false
      o.old = {}
      o.old.fill = o.fill
      o.old.evented = o.evented
      o.old.stroke = o.stroke
      o.set('fill', 'yellow')
      o.set('evented', true)
    })
  }

  static renderSpots(initial = false) {

    var popCount = 0;
    var canvas = MasterController.canvasInAction
    var all_spots = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id && o.meta.type === 'spot')

    canvas._objects.filter(o => o.meta && o.meta.type === 'unit').map(o => o.canvas.remove(o))

    for (var i = 0; i < all_spots.length; i++) {

      var obj = all_spots[i];
      var _ID = obj.meta ? obj.meta : null;
      var _ID = _ID ? _ID.id : null;
      var _top = obj.top + obj.height / 2;
      var _left = obj.left + obj.width / 2;

      obj.set({
        selectable: false,
        evented: true,
        width: initial ? obj.width - 2 : obj.width,
        height: initial ? obj.height - 2 : obj.height,
        strokeWidth: 0.5,
      })

      if (obj.meta && obj.meta.id) {

        obj.set({
          // shadow:{color: 'rgba(0,0,0,0.3)', offsetX:2,offsetY:0, blur:2},
          stroke: Constants.INACTIVE_STROKE_COLOR,
          strokeWidth: Constants.INACTIVE_STROKE_WIDTH,
          opacity: 1,

        });


        var unit_fill_color = Constants.COLORS.STATIC;
        if (obj.unit) {
          unit_fill_color = (obj.unit.status === 'Loading') ? Constants.COLORS.UNIT_LOADING : Constants.COLORS.UNIT_UNLOADING;

          var unit = new fabric.Rect({
            originX: 'center',
            originY: 'top',
            top: (obj.aCoords.tl.y + obj.aCoords.tr.y) / 2,
            left: (obj.aCoords.tl.x + obj.aCoords.tr.x) / 2,
            width: 0.85 * obj.width,
            height: 10,
            fill: unit_fill_color,
            angle: obj.angle,
            opacity: obj.unit ? 1 : 0,
            evented: false,
            meta: {
              type: 'unit',
              interactive: false,
            }
          })
          canvas.add(unit);

        }




        if (obj.status === 'Available') {
          obj.set({
            fill: Constants.COLORS.AVAILABLE
          });
        } else if (obj.status === 'Closed') {
          obj.set({
            fill: Constants.COLORS.CLOSED
          });
        } else if (obj.status === 'Occupied') {
          obj.set({
            fill: Constants.COLORS.OCCUPIED
          });
        } else if (obj.status === 'Booked') {
          obj.set({
            fill: Constants.COLORS.BOOKED
          });
        }
        if (obj.ID === 0) {
          obj.set({
            fill: Constants.COLORS.STATIC,
            shadow: {
              color: 'rgba(0,0,0,0.35)',
              offsetX: 3,
              offsetY: 0,
              blur: 5,
              evented: false,
            }
          });

        }


      } else {
        if (obj.type !== 'text' && !(obj.meta && obj.meta.type === 'unit')) {
          obj.set({
            status: null,
            evented: false,
            unitNum: null,
            fill: Constants.COLORS.STATIC,
            stroke: Constants.COLORS.BORDER_DARK,
          });

        }
      }

    }

    canvas.requestRenderAll()

  }

}