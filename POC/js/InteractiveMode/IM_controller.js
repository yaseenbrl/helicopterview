class IM_controller {

  static interactiveCanvas = null;
  static dynamicZoomCanvas = null
  static interactiveToolbarCanvas = null;
  static viewRectangle = null

  static dynamicPointer = null
  static dynamicZoomOn = true;

  static MultiZoomActive = false;



  static switchDynamicZoom() {
    if (IM_controller.dynamicZoomOn) {
      IM_controller.dynamicZoomOn = false
      document.getElementById("dynamicZoomCanvas").style.visibility = 'hidden'
      document.getElementsByClassName("dynamicZoomView")[0].style.height = '10px'
      IM_controller.viewRectangle.opacity = 0
    } else {
      IM_controller.dynamicZoomOn = true
      document.getElementById("dynamicZoomCanvas").style.visibility = 'visible'
      document.getElementsByClassName("dynamicZoomView")[0].style.height = '200px'
      IM_controller.viewRectangle.opacity = 1
    }
    IM_controller.interactiveCanvas.requestRenderAll();

  }
  static init() {

    var containterWidth = document.getElementsByClassName("interactiveMode")[0].offsetWidth - 50; // subtract toolbar width
    var containterHeight = document.getElementsByClassName("interactiveMode")[0].offsetHeight;


    var dynamicZoomViewWidth = document.getElementsByClassName("dynamicZoomView")[0].offsetWidth;
    var dynamicZoomViewHeight = document.getElementsByClassName("dynamicZoomView")[0].offsetHeight - document.getElementById("dynamicZoomHeader").offsetHeight;


    if (MasterController.interactiveCanvas === null) {
      MasterController.addDocumentEventListners()

      MasterController.interactiveCanvas = new fabric.Canvas('InteractiveCanvas');
      MasterController.interactiveCanvas.backgroundColor = Constants.COLORS.CANVAS_BACKGROUND; //!@#
      MasterController.interactiveCanvas.setHeight(containterHeight);
      MasterController.interactiveCanvas.setWidth(containterWidth);
      MasterController.interactiveCanvas.requestRenderAll();
      IM_controller.interactiveCanvas = MasterController.interactiveCanvas

      MasterController.interactiveCanvas.on('mouse:wheel', IM_viewActionsHandler.zoom_wheel);
      MasterController.interactiveCanvas.on('mouse:down', IM_viewActionsHandler.pan_mousedown);
      MasterController.interactiveCanvas.on('mouse:move', IM_viewActionsHandler.pan_mousemove);
      MasterController.interactiveCanvas.on('mouse:up', IM_viewActionsHandler.pan_mouseup);
      //
      // MasterController.interactiveCanvas.on('mouse:down', IM_drawingActionsHandler.drawPolygon_mousedown);
      // MasterController.interactiveCanvas.on('mouse:move', IM_drawingActionsHandler.drawPolygon_mousemove);
      // MasterController.interactiveCanvas.on('mouse:dblclick', IM_drawingActionsHandler.drawPolygon_mousedblclick);
      //
      // MasterController.interactiveCanvas.on('mouse:down', IM_drawingActionsHandler.drawTile_mousedown);
      // MasterController.interactiveCanvas.on('mouse:move', IM_drawingActionsHandler.drawTile_mousemove);

      MasterController.interactiveCanvas.on('mouse:up', MasterController._showIDs);
      MasterController.interactiveCanvas.on('mouse:move', IM_viewActionsHandler.updateViewRectangle_mousemove);

      MasterController.interactiveCanvas.on('mouse:over', IM_viewActionsHandler.stroke_mouseover);
      MasterController.interactiveCanvas.on('mouse:out', IM_viewActionsHandler.unstroke_mouseout);

      MasterController.interactiveCanvas.on('mouse:over', IM_viewActionsHandler.showInfo_mouseover);
      MasterController.interactiveCanvas.on('mouse:out', IM_viewActionsHandler.hideInfo_mouseout);

      MasterController.interactiveCanvas.on('mouse:up', IM_spotsActionsHandler.showDialogBox_mouseup);


      // MasterController.interactiveCanvas.on('mouse:up', IM_viewActionsHandler.broadcastCanvas_mouseup);




      IM_controller.dynamicZoomCanvas = new fabric.StaticCanvas('dynamicZoomCanvas');
      IM_controller.dynamicZoomCanvas.backgroundColor = Constants.COLORS.CANVAS_BACKGROUND;
      IM_controller.dynamicZoomCanvas.setHeight(dynamicZoomViewHeight);
      IM_controller.dynamicZoomCanvas.setWidth(dynamicZoomViewWidth);
      IM_controller.dynamicZoomCanvas.requestRenderAll();


      IM_controller.dynamicZoomCanvas.on('object:added', function(e) {

        e.target.set({
          selectable: false,
          evented: false
        })
        if (e.target.meta && e.target.meta.id == 'viewRectangle') {
          IM_controller.dynamicZoomCanvas.remove(e.target)
        }
      });



      IM_controller.viewRectangle = new fabric.Rect({
        fill: 'rgba(0,0,0,0)',
        opacity: 1,
        stroke: Constants.COLORS.viewRectangle,
        strokeWidth: 4,
        evented: false,
        selectable: false,
        meta: {
          id: 'viewRectangle'
        },

      });

      IM_controller.viewRectangle.original_strokeWidth = IM_controller.viewRectangle.strokeWidth
      IM_controller.interactiveCanvas.add(IM_controller.viewRectangle)
      IM_viewActionsHandler.renderViewRectangle();

      MasterController.canvasInAction = IM_controller.interactiveCanvas


      MasterController.interactiveToolbarCanvas = new fabric.Canvas('InteractiveToolbar');
      MasterController.interactiveToolbarCanvas.setHeight(containterHeight);
      MasterController.interactiveToolbarCanvas.backgroundColor = Constants.COLORS.TOOLBAR_BACKGROUND;
      MasterController.interactiveToolbarCanvas.requestRenderAll();
      IM_controller.interactiveToolbarCanvas = MasterController.interactiveToolbarCanvas

      IM_toolbarHandler.loadLeftToolbar(this.interactiveToolbarCanvas)

    }



  }



}