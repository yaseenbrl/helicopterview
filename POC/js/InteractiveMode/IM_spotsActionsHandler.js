class IM_spotsActionsHandler {
  static currentAction = null; // keeps track of the actions transmitted from the dialogBox


  static moveUnitStart(id) {
    document.getElementById('dialogWrapper').remove()
    IM_spotsActionsHandler.currentAction = 'MOVE_UNIT'
    IM_viewActionsHandler.filterSpots("Available")
    var obj = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id === id)[0]
    IM_viewActionsHandler.showInfoBox("Moving Unit " + obj.unit.num + " from spot " + id)


  }
  static moveUnitEnd(source_id, dist_id) {
    var source_obj = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id === source_id)[0]
    var dist_obj = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id === dist_id)[0]

    dist_obj.unit = source_obj.unit
    dist_obj.status = source_obj.status
    source_obj.status = 'Available'
    source_obj.unit = null
    IM_viewActionsHandler.unfilterSpots()
    IM_viewActionsHandler.renderSpots(false)
    IM_viewActionsHandler.hideInfoBox()
    IM_controller.interactiveCanvas.requestRenderAll()
    IM_viewActionsHandler.refreshDynamicZoomCanvas(null, true)



  }


  static showDialogBox_mouseup(opts) {

    if (opts.e.altKey === false) {
      if (opts.target && opts.target.meta && opts.target.meta.type == 'spot') {
        var title = opts.target.meta.id
        var actions = []
        if (opts.target.status === 'Occupied') {
          actions.push({
            name: "Move unit ",
            handler: "IM_spotsActionsHandler.moveUnitStart(" + opts.target.id + ")"
          })
        } else if (opts.target.status === 'Available') {
          actions.push({
            name: "Book",
            handler: "IM_spotsActionsHandler.bookSpot()"
          })
        } else if (opts.target.status === 'Booked') {
          actions.push({
            name: "Unbook",
            handler: "IM_spotsActionsHandler.unbookSpot()"
          })
        }

        DialogBoxHandler.showDialogBox(title, actions)
      }
    }

  }

}