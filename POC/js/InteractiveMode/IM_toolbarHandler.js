class IM_toolbarHandler {

  static tools = [];


  static addTool(image, toolBar, selectionHandler, order, backgroundOn = true) {
    console.log("running  addTool")

    fabric.Image.fromURL(image, function(oImg) {
      var rect = new fabric.Rect({
        selectable: false,
        evented: false
      })

      oImg.set({
        left: toolBar.width / 2 - 16,
        top: (32 * order),
        width: 32,
        height: 32,
        angle: 0,
        opacity: 1,
        selectable: true,
        stroke: 'rgba(0,0,0,0)',
        strokeWidth: 2,
        borderColor: '#464646',
        borderScaleFactor: 1,
        lockMovementX: true,
        lockMovementY: true,
        hasControls: false,
        bgRect: rect,
      })
      rect.set({
        width: oImg.width * 1.5,
        height: oImg.height * 1.5,
        top: oImg.getCenterPoint().y,
        left: oImg.getCenterPoint().x,
        fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE,
        rx: 10,
        ry: 10,
        originX: 'center',
        originY: 'center',
        opacity: backgroundOn ? 1 : 0,
      })

      toolBar.add(rect);

      toolBar.add(oImg);


      oImg.on('selected', function() {
        selectionHandler();
        var activeObject = toolBar.getActiveObject()
        if (activeObject) {
          // activeObject.set({
          //   stroke: '#f00',
          //   strokeWidth: 2,
          // })

          oImg.bgRect.set({
            fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_ACTIVE
          })

        }
      });

      oImg.on('deselected', function() {
        // oImg.set({
        //   stroke: 'rgba(0,0,0,0)',
        //   strokeWidth: 2,
        // })

        oImg.bgRect.set({
          fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE
        })
      });

      oImg.on('mouseover', function() {
        oImg.animate('scaleX', 1.05, {
          duration: 50,
          onChange: toolBar.renderAll.bind(toolBar),
        });
        oImg.animate('scaleY', 1.05, {
          duration: 50,
          onChange: toolBar.renderAll.bind(toolBar),
        });
        oImg.bgRect.set({
          fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_ACTIVE
        })
      });
      oImg.on('mouseout', function() {
        oImg.animate('scaleX', 1.0, {
          duration: 50,
          onChange: toolBar.renderAll.bind(toolBar),
        });
        oImg.animate('scaleY', 1.0, {
          duration: 50,
          onChange: toolBar.renderAll.bind(toolBar),
        });
        if (toolBar.getActiveObject() !== oImg) {
          oImg.bgRect.set({
            fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE
          })
        }

      });

      IM_toolbarHandler.tools.push(oImg);
    });

    toolBar.requestRenderAll();
  }


  static importYard() {
    MasterController.getFileContent(IM_toolbarHandler.loadYardFromJSON);
  }

  // @public
  static loadYardFromJSON(json) {
    var parsed_json = JSON.parse(json)
    var viewportTransform = Object.keys(parsed_json).includes('viewportTransform') ? parsed_json['viewportTransform'] : [1, 0, 0, 1, 0, 0]
    MasterController.canvasInAction.loadFromJSON(parsed_json, () => {
      MasterController.interactiveCanvas._objects.filter(o => !(o.meta && o.meta.id === 'viewRectangle')).map(o => o.set({
        fill: Constants.COLORS.STATIC,
        evented: false,
        selectable: false,
      }))
      IM_toolbarHandler.populateMockData()
      IM_viewActionsHandler.renderSpots(true)
      MasterController.interactiveCanvas.backgroundColor = Constants.COLORS.BACKGROUND;
      MasterController._showIDs()
      MasterController.interactiveCanvas.viewportTransform = viewportTransform
      MasterController.interactiveToolbarCanvas.discardActiveObject().requestRenderAll();
      MasterController.interactiveCanvas.requestRenderAll()
      IM_viewActionsHandler.refreshDynamicZoomCanvas(null, true);

    })
  }


  static generateSpotsData() {
    var IDs = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id).map(o => o.meta.id)
    var spotStatuses = ["Closed", "Closed", "Closed", "Closed", "Occupied", "Occupied", "Booked", "Booked", "Available", "Available", "Available"]
    var unitStatuses = ["Loading", "Unloading"]
    var sampleInfo = ["This is a sampleInfo #1", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "This is another, longer sampleInfo that can occupy multiple lines."]
    var data = new Object();
    for (var i = 0; i < IDs.length; i++) {
      var _rand = Math.floor(Math.random() * spotStatuses.length)
      data[IDs[i]] = {
        status: spotStatuses[_rand],
        unit: (spotStatuses[_rand] === "Occupied" || spotStatuses[_rand] === "Booked") ? {
          status: unitStatuses[Math.floor(Math.random() * unitStatuses.length)],
          num: Math.floor(Math.random() * 100000000),
          info: sampleInfo[Math.floor(Math.random() * sampleInfo.length)],
        } : null,
      }
    }

    return data

  }


  static populateMockData() {
    var json = IM_toolbarHandler.generateSpotsData()

    var all_spots = MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id && o.meta.type === 'spot')

    for (var i = 0; i < all_spots.length; i++) {
      var obj = all_spots[i];
      var _ID = obj.meta ? obj.meta : null;
      var _ID = _ID ? _ID.id : null;
      if (_ID in json) {

        obj.set({
          info: json[_ID],
          status: json[_ID].status,
          unit: json[_ID].unit ? json[_ID].unit : null,
        });

      }


    }

  }


  static importZoom() {
    MasterController.zoomViews = []

    MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id && o.meta.id.includes('zoom')).forEach(o => MasterController.canvasInAction.remove(o))
    MasterController.postMessage({
      'type': 'clear'
    })
    MasterController.getFileContent(_ => {

      var objects = JSON.parse(_)['objects']
      objects.forEach(o => {
        var _rect = new fabric.Rect(o);
        _rect.set('fill', 'rgba(0,0,0,0)')
        _rect.set('evented', false)
        _rect.set('selectable', false)
        MasterController.zoomViews.push(_rect);
        MasterController.canvasInAction.add(_rect);
        MasterController.postMessage({
          'type': 'zoomview',
          'data': o
        })
      })





      MasterController.canvasInAction.requestRenderAll();
      MasterController.interactiveToolbarCanvas.discardActiveObject().requestRenderAll();

      IM_controller.MultiZoomActive = true

      MasterController.postMessage({
        'type': 'canvasJSON',
        'data': MasterController.canvasInAction.toJSON(["meta", "info"])
      })

    })

  }


  static openZoomView() {
    var _window = window.open('./zooms.html', '_blank');
    MasterController.interactiveToolbarCanvas.discardActiveObject().requestRenderAll();
    return _window
  }


  static loadLeftToolbar(toolbar_canvas) {
    console.log("running  loadLeftToolbar");

    toolbar_canvas.backgroundColor = Constants.COLORS.TOOLBAR_BACKGROUND;

    IM_toolbarHandler.addTool('./assets/icons/openZoomView.png', toolbar_canvas, IM_toolbarHandler.openZoomView, 10)
    IM_toolbarHandler.addTool('./assets/icons/import-zoom.png', toolbar_canvas, IM_toolbarHandler.importZoom, 2)
    IM_toolbarHandler.addTool('./assets/icons/import-yard.png', toolbar_canvas, IM_toolbarHandler.importYard, 5)






    toolbar_canvas.requestRenderAll()
  }


}