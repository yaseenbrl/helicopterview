class MultiZoomViewsController {
  static zoomViews = []
  static broadcastChannel = new BroadcastChannel('staticZoomsChannel');
  static masterCanvas = null

  static init() {
    console.log("initializing MultiZoomViewsController")
    MultiZoomViewsController.broadcastChannel.onmessage = function(ev) {
      MultiZoomViewsController.newMessageHandler(ev)
    }
  }

  static createCanvas(id, zoomLevel) {
    var canvas = new fabric.StaticCanvas(id);
    var containterHeight = document.getElementById(id).parentElement.parentElement.offsetHeight
    var containterWidth = document.getElementById(id).parentElement.parentElement.offsetWidth
    canvas.setHeight(containterHeight);
    canvas.setWidth(containterWidth);
    canvas.setZoom(zoomLevel);
    canvas.backgroundColor = Constants.COLORS.CANVAS_BACKGROUND;
    canvas.requestRenderAll();
    return canvas
  }

  static _showIDs_beta() {

    var color = 'black'
    var objects = MultiZoomViewsController.masterCanvas._objects;
    var toBeAdded = [];
    var toBeRemoved = [];

    for (var i = 0; i < objects.length; i++) {
      if (objects[i].type === 'text') {
        toBeRemoved.push(objects[i])
      }
    }

    for (var i = 0; i < toBeRemoved.length; i++) {
      MultiZoomViewsController.masterCanvas.remove(toBeRemoved[i]);
    }

    for (var i = 0; i < objects.length; i++) {




      var center_point = objects[i].aCoords.bl

      if (objects[i].meta && objects[i].meta.id) {
        if (objects[i].meta.id.includes('zoom') || objects[i].meta.id.includes('view')) {
          continue;
        }

        var textObj = new fabric.Text(objects[i].meta.id, {
          fill: color,
          fontSize: Math.min(15, 0.18 * Math.max(objects[i].width, objects[i].height)),
          left: center_point.x,
          top: center_point.y,
          originX: 'left',
          originY: 'top',
          selectable: false,
          evented: false,
          angle: (objects[i].angle - 90) < -90 ? 180 + (objects[i].angle - 90) : objects[i].angle - 90,
          fontFamily: Constants.FONT_FAMILY,
        })

        if (objects[i].info && objects[i].info.unit && objects[i].info.unit.num) {

          var center_point_x = textObj.angle ? (objects[i].aCoords.br.x) : objects[i].aCoords.br.x + 5
          var center_point_y = objects[i].aCoords.br.y


          var unitNumTxt = new fabric.Text(objects[i].info.unit.num.toString(), {
            fill: color,
            fontSize: Math.min(10, 0.14 * Math.max(objects[i].width, objects[i].height)),
            left: center_point_x - 2,
            top: center_point_y - 2,
            originX: 'left',
            originY: 'bottom',
            selectable: false,
            evented: false,
            angle: (objects[i].angle - 90) < -90 ? 180 + (objects[i].angle - 90) : objects[i].angle - 90,
            fontFamily: Constants.FONT_FAMILY,
          })
          toBeAdded.push(unitNumTxt)

        }

        toBeAdded.push(textObj)
      }
    }


    for (var i = 0; i < toBeAdded.length; i++) {
      MultiZoomViewsController.masterCanvas.add(toBeAdded[i]);
    }

  }

  static newMessageHandler(ev) {

    var data = JSON.parse(LZString.decompress(ev.data))

    switch (data.type) {
      case 'clear':

        document.getElementById("main_wrapper").innerHTML = "";
        MultiZoomViewsController.zoomViews = []
        MultiZoomViewsController.masterCanvas = null

        break;

      case 'zoomview':

        var magnification = data.data.meta.magnification
        var tabScalingFactor = data.data.meta.tabScalingFactor
        var zoomLevel = magnification / tabScalingFactor
        var left = data.data.meta.tabPositionX * data.data.meta.windowOuterWidth
        var top = data.data.meta.tabPositionY * data.data.meta.windowOuterHeight
        var tabWidth = (data.data.meta.tabWidth * data.data.meta.windowOuterWidth + 2 * data.data.strokeWidth).toFixed(2)
        var tabHeight = (data.data.meta.tabHeight * data.data.meta.windowOuterHeight + 2 * data.data.strokeWidth).toFixed(2)
        var html_to_insert = `
        <div class="draggableDiv" style = "left:` + left + `px; top: ` + top + `px;">
          <div class="draggableDivheader" style="background-color: ` + data.data.stroke + `;"></div>
          <div class="resizable" style="width: ` + tabWidth + `px; height: ` + tabHeight + `px" ><canvas id="` + data.data.meta.id + `"></div>
        </div>
        `






        document.getElementById("main_wrapper").insertAdjacentHTML('beforeend', html_to_insert);
        var list = document.getElementsByClassName("draggableDiv")
        for (let item of list) {
          dragElement(item)
        }


        MultiZoomViewsController.zoomViews.push({
          'id': data.data.meta.id,
          'top': -(data.data.top - (0.5 * data.data.strokeWidth)) * zoomLevel,
          'left': -(data.data.left - (0.5 * data.data.strokeWidth)) * zoomLevel,
          'zoomLevel': zoomLevel,
          'tabScalingFactor': data.data.meta.tabScalingFactor,
          'canvas': MultiZoomViewsController.createCanvas(data.data.meta.id, zoomLevel)
        })
        break;
      case 'canvasJSON':
        var _ = document.createElement('canvas');
        _.setAttribute("id", "masterCanvas");
        MultiZoomViewsController.masterCanvas = new fabric.StaticCanvas('masterCanvas')
        MultiZoomViewsController.masterCanvas.loadFromJSON(data.data)
        MultiZoomViewsController.masterCanvas._objects.filter(obj => obj.meta && obj.meta.id && (obj.meta.id.includes('view') || obj.meta.id.includes('zoom'))).forEach(_obj => _obj.canvas.remove(_obj))
        MultiZoomViewsController.masterCanvas._objects.filter(obj => obj.type === 'text').forEach(_obj => _obj.canvas.remove(_obj))

        MultiZoomViewsController.masterCanvas._objects.map(o => o.set({
          selectable: false,
          evented: false
        }))
        MultiZoomViewsController._showIDs_beta()
        var json = MultiZoomViewsController.masterCanvas.toJSON(["meta", "info"])

        MultiZoomViewsController.zoomViews.forEach(zoomView => zoomView.canvas.loadFromJSON(json, () => {
          zoomView.canvas.viewportTransform[4] = zoomView.left

          zoomView.canvas.viewportTransform[5] = zoomView.top
          zoomView.canvas.requestRenderAll()
        }))





        break;
      case 'stroke_mouseover':
        MultiZoomViewsController.zoomViews.forEach(zoomView => {

          var obj = zoomView.canvas._objects.filter(o => o.meta && o.meta.id === data.data.id)[0]
          obj.set('stroke', Constants.ACTIVE_STROKE_COLOR);
          obj.set('strokeWidth', Constants.ACTIVE_STROKE_WIDTH);
          zoomView.canvas.requestRenderAll()
        })
        break;









      case 'unstroke_mouseout':
        MultiZoomViewsController.zoomViews.forEach(zoomView => {

          var obj = zoomView.canvas._objects.filter(o => o.meta && o.meta.id === data.data.id)[0]
          obj.set('stroke', Constants.INACTIVE_STROKE_COLOR);
          obj.set('strokeWidth', Constants.INACTIVE_STROKE_WIDTH);
          zoomView.canvas.requestRenderAll()
        })

    }





  }
}


function dragElement(elmnt) {

  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  if (document.getElementById("draggableDivheader")) {

    document.getElementById("draggableDivheader").onmousedown = dragMouseDown;
  } else {

    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {

    e = e || window.event;
    e.preventDefault();

    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;

    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {

    e = e || window.event;
    e.preventDefault();

    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;

    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {


    document.onmouseup = null;
    document.onmousemove = null;
  }
}



window.onload = function() {
  MultiZoomViewsController.init()
  var list = document.getElementsByClassName("draggableDiv")
  for (let item of list) {
    dragElement(item)
  }




}