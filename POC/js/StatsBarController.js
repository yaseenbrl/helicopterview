class StatsBarController {
  static statsTitles = ["incoming actual", "actual unloaded at dock", "total occupied parking spots", "scheduled outgoing & un-assigned", "loaded at dock", "parked"]
  static loadTypes = ["normal trailer", "jumbo trailer", "mega trailer", "normal swap body", "jumbo swap body", "half swap body", "fixed box", "mini trailer", "van 3.5t", "van 7.5t", "double decker"]
  static spotTypes = ['PARKING', 'LOADING', 'UNLOADING', 'LOADING_UNLOADING']
  static render(mode) {
    var statsBarDiv = document.getElementsByClassName("stats_bar")[0]
    if (mode === 'INTERACTIVE') {
      statsBarDiv.innerHTML = ""
      statsBarDiv.style.visibility = 'visible'
      StatsBarController.statsTitles.forEach((item, i) => {

        var value = Math.floor(Math.random() * 100)
        var html_to_insert = "&nbsp ".repeat(7) + item + ` : <span onclick="StatsBarController.statClick(this)" style=" cursor: pointer; color:#000 " class="statValue" data = "` + item + `">` + value + `</span>`
        statsBarDiv.insertAdjacentHTML('beforeend', html_to_insert);

      });
      var search_box = "&nbsp ".repeat(7) + 'Search: <input id = "search_box" type="search" onkeyup=IM_viewActionsHandler.searchBox_handler()>';
      statsBarDiv.insertAdjacentHTML('beforeend', search_box);
      $('input[type=search]').on('search', function() {
        IM_viewActionsHandler.searchBox_handler()
      });
    } else if (mode === 'DRAWING') {


      statsBarDiv.style.visibility = 'visible'

      statsBarDiv.innerHTML = `<table>
  <tr>
    <th>Name:</th>
    <td>Bill Gates</td>
  </tr>
`
      var _html = "<table>  <tr>    <th>Area Load Types </th>"
      StatsBarController.loadTypes.forEach((item) => {
        _html += `<td><input class = "loadTypes" type="checkbox" name="loadType" value="` + item + `">  `
        _html += `<label for="` + item + `"> ` + item + `</label> </td>`
      })
      _html += "</tr>"

      _html += "<tr>    <th>Yard Type </th>"
      StatsBarController.spotTypes.forEach((item) => {
        _html += `<td><input class = "spotType" type="radio" name="spotType" value="` + item + `" checked>  `
        _html += `<label for="` + item + `"> ` + item + `</label> </td>`
      })
      _html += "</tr></table>"

      statsBarDiv.innerHTML = _html
      // statsBarDiv.innerHTML = "<strong>Load Types :   </strong>"
      // StatsBarController.loadTypes.forEach((item)=>{
      //    var html_to_insert =`<input class = "loadTypes" type="checkbox" name="loadType" value="`+item+`">  `
      //   html_to_insert+= `<label for="`+item+`"> `+item+`</label> ` + "&nbsp ".repeat(2)
      //   statsBarDiv.insertAdjacentHTML('beforeend', html_to_insert);
      // })

      // statsBarDiv.insertAdjacentHTML('beforeend', "<br>");
      // statsBarDiv.insertAdjacentHTML('beforeend', "<strong>Spot Type  &nbsp :   </strong>");
      //
      // StatsBarController.spotTypes.forEach((item)=>{
      //   var html_to_insert = `<input class = "spotType" type="radio" name="spotType" value="`+item+`" checked>  `
      //  html_to_insert+= `<label for="`+item+`"> `+item+`</label> ` + "&nbsp ".repeat(2)
      //  statsBarDiv.insertAdjacentHTML('beforeend', html_to_insert);
      //
      // });

    } else {
      statsBarDiv.innerHTML = ""
      statsBarDiv.style.visibility = 'hidden'
    }

    statsBarDiv.insertAdjacentHTML('beforeend', `<div id="infoBar"></div>`);


  }
  static statClick(o) {
    alert("click on " + o.attributes.data.textContent)
  }
}