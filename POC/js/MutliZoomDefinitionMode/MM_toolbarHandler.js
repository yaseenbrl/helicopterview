class MM_toolbarHandler {

  static tools = [];


  static defineZoomViewModeOn() {
    console.log("running  defineZoomViewModeOn");
    if (MM_controller.zoomViews.length >= Constants.MAX_ALLOWED_ZOOMVIEWS) {
      alert("You've reached the maximum allowed number of zoom views")
      return
    } else {
      MM_controller.defineZoomViewMode = true;
    }

    // canvasChangeCursor("crosshair");

  }


  static addTool(image, toolBar, selectionHandler, order, backgroundOn = true, name = "test") {
    console.log("Adding tool: " + name)



    fabric.Image.fromURL(image, function(oImg) {
      var rect = new fabric.Rect({
        selectable: false,
        evented: false
      })
      oImg.set({
        left: toolBar.width / 2 - 16,
        top: (40 * order) - 15,
        width: 32,
        height: 32,
        angle: 0,
        opacity: 1,
        selectable: true,
        stroke: 'rgba(0,0,0,0)',
        strokeWidth: 2,
        borderColor: '#464646',
        borderScaleFactor: 1,
        lockMovementX: true,
        lockMovementY: true,
        hasControls: false,
        bgRect: rect,
      })
      rect.set({
        width: oImg.width * 1.5,
        height: oImg.height * 1.5,
        top: oImg.getCenterPoint().y,
        left: oImg.getCenterPoint().x,
        fill: name !== 'Assign Off' ? Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE : '#eee',
        rx: 10,
        ry: 10,
        originX: 'center',
        originY: 'center',
        opacity: backgroundOn ? 1 : 0,
      })
      var textObj = new fabric.Text(name, {
        fill: Constants.COLORS.TOOLBAR_TEXT_COLOR,
        fontSize: 13,
        left: rect.left,
        top: rect.top + rect.height / 2 + 10,
        originX: 'center',
        originY: 'center',
        selectable: false,
        evented: false,
        _id: name,
        fontFamily: Constants.FONT_FAMILY,
      })
      console.log("ADDED " + name)

      toolBar.add(rect);
      toolBar.add(oImg);
      toolBar.add(textObj);


      oImg.on('selected', function() {
        selectionHandler();
        var activeObject = toolBar.getActiveObject()
        if (activeObject) {
          // activeObject.set({
          //   stroke: '#f00',
          //   strokeWidth: 2,
          // })

          oImg.bgRect.set({
            fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_ACTIVE
          })

        }
      });

      oImg.on('deselected', function() {
        // oImg.set({
        //   stroke: 'rgba(0,0,0,0)',
        //   strokeWidth: 2,
        // })

        oImg.bgRect.set({
          fill: name !== 'Assign Off' ? Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE : '#eee'
        })
      });

      oImg.on('mouseover', function() {
        // oImg.animate('scaleX', 1.05, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        // oImg.animate('scaleY', 1.05, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        oImg.bgRect.set({
          fill: Constants.COLORS.TOOLBAR_RECT_BACKGROUND_ACTIVE
        })
        toolBar.requestRenderAll();
      });
      oImg.on('mouseout', function() {
        // oImg.animate('scaleX', 1.0, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        // oImg.animate('scaleY', 1.0, {
        //   duration: 50,
        //   onChange: toolBar.renderAll.bind(toolBar),
        // });
        if (toolBar.getActiveObject() !== oImg) {
          oImg.bgRect.set({
            fill: name !== 'Assign Off' ? Constants.COLORS.TOOLBAR_RECT_BACKGROUND_INACTIVE : '#eee'
          })
          toolBar.requestRenderAll();
        }

      });

      //dirty if statement to accomodate new requirements
      if (name == 'Assign Off') {
        try {
          toolBar.remove(toolBar._objects.filter(o => o._id && o._id === 'Assign On')[0])
        } catch (e) {
          console.log(e)
        }

      } else if (name == 'Assign On') {
        try {
          toolBar.remove(toolBar._objects.filter(o => o._id && o._id === 'Assign Off')[0])
        } catch (e) {
          console.log(e)
        }
      }

      DM_toolbarHandler.tools.push(oImg);
    });

    toolBar.requestRenderAll();
  }



  static setCanvasBackground() {
    console.log("Setting background")

    var imgInput = document.createElement('input');
    imgInput.type = "file";

    imgInput.onsubmit = function(e) {
      MasterController.mzdToolbarCanvas.discardActiveObject().requestRenderAll();
    }

    imgInput.onchange = function(e) {

      var url = URL.createObjectURL(e.target.files[0]);
      fabric.Image.fromURL(url, function(img) {
        var oImg = img.set({
          left: 0,
          top: 0,
          selectable: false,
        })

        // normalize size
        oImg.set({
          scaleX: 2 * Math.min(MasterController.canvasInAction.width / oImg.width, MasterController.canvasInAction.height / oImg.height),
          scaleY: 2 * Math.min(MasterController.canvasInAction.width / oImg.width, MasterController.canvasInAction.height / oImg.height)
        })

        // delete current background, if it exists [removes all canvas objects of type 'image' ]
        for (var i = 0; i < MasterController.canvasInAction._objects.length; i++) {
          if (MasterController.canvasInAction._objects[i].type === 'image') {
            MasterController.canvasInAction.remove(MasterController.canvasInAction._objects[i])
          }
        }

        MasterController.canvasInAction.add(oImg)
        MasterController.canvasInAction.sendToBack(oImg);
        MasterController.canvasInAction.requestRenderAll()


        MasterController.mzdToolbarCanvas.discardActiveObject().requestRenderAll();

      });


    }

    $(imgInput).click();

  }


  static upload() {

    var _json = MasterController.canvasInAction.toJSON("meta");
    var relevant_objects = _json['objects'].filter(o => o.meta && o.meta.id && o.meta.id.includes('zoom'));

    _json['objects'] = relevant_objects

    MasterController.saveToFile(JSON.stringify(_json), "json", "zoom");
    MasterController.mzdToolbarCanvas.discardActiveObject().requestRenderAll();
  }

  static downloadZoom() {

    MasterController.canvasInAction._objects.filter(o => o.meta && o.meta.id && o.meta.id.includes('zoom')).forEach(o => MasterController.canvasInAction.remove(o))
    MasterController.getFileContent(_ => {
      MM_controller.mzdTabviewCanvas.clear()
      var objects = JSON.parse(_)['objects']
      objects.forEach(o => {
        var _rect = new fabric.Rect(o);
        var tabViewRepresentative = new fabric.Rect({
          height: _rect.height * _rect.meta.magnification,
          width: _rect.width * _rect.meta.magnification,
          magnification: _rect.meta.magnification,
          fill: _rect.stroke,
          stroke: 'black',
          strokeUniform: true,
          strokeWidth: 2,
          lockScalingFlip: true,
          objectCaching: false,
          lockRotation: true,
          // originX: 'center',
          // originY: 'center',
          top: _rect.meta.tabPositionY * MM_controller.mzdTabviewCanvas.height,
          left: _rect.meta.tabPositionX * MM_controller.mzdTabviewCanvas.width,
          parent: _rect
        })
        _rect.child = tabViewRepresentative

        MM_controller.zoomViews.push(_rect);
        MasterController.canvasInAction.add(_rect);
        MasterController.mzdTabviewCanvas.add(tabViewRepresentative);
      })
      MasterController.canvasInAction.requestRenderAll();
      MasterController.mzdTabviewCanvas.requestRenderAll();
      MasterController.mzdToolbarCanvas.discardActiveObject().requestRenderAll();

    })

  }

  static download() {

    MasterController.getFileContent(_ => {
      MasterController.canvasInAction.loadFromJSON(JSON.parse(_), () => {

        MasterController.canvasInAction._objects.forEach(obj => {
          obj.set({
            fill: 'grey',
            opacity: 1,
            selectable: false,
            evented: false
          })
        })
        MasterController._showIDs()
        MasterController.canvasInAction.requestRenderAll();
        MasterController.mzdToolbarCanvas.discardActiveObject().requestRenderAll();

      })
    })

  }




  static loadLeftToolbar(toolbar_canvas) {
    console.log("running  loadLeftToolbar");

    toolbar_canvas.backgroundColor = Constants.COLORS.TOOLBAR_BACKGROUND;

    MM_toolbarHandler.addTool('./assets/icons/newZoomView.png', toolbar_canvas, MM_toolbarHandler.defineZoomViewModeOn, 1, true, "Draw")
    MM_toolbarHandler.addTool('./assets/icons/import-zoom.png', toolbar_canvas, MM_toolbarHandler.downloadZoom, 3, true, "Import")

    MM_toolbarHandler.addTool('./assets/icons/export-zoom.png', toolbar_canvas, MM_toolbarHandler.upload, 5, true, "Export")
    MM_toolbarHandler.addTool('./assets/icons/import-yard.png', toolbar_canvas, MM_toolbarHandler.download, 7, true, "Upload")
    // MM_toolbarHandler.addTool('./assets/icons/blueprint.png', toolbar_canvas, MM_toolbarHandler.setCanvasBackground, 9)


    toolbar_canvas.requestRenderAll()
  }


}