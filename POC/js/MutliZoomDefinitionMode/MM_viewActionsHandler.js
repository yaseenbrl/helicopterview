class MM_viewActionsHandler {

  static zoom_wheel(opts) {

    if (opts.e.altKey === true) {
      var delta = opts.e.deltaY;
      var zoom = this.getZoom();
      zoom *= 0.999 ** delta;
      if (zoom > 20) zoom = 20;
      if (zoom < 0.01) zoom = 0.01;

      MasterController.canvasInAction.zoomToPoint({
        x: opts.e.offsetX,
        y: opts.e.offsetY
      }, zoom);


      opts.e.preventDefault();
      opts.e.stopPropagation();
    }
    this.requestRenderAll()
  }


  static pan_mousedown(opts) {
    var evt = opts.e;
    if (evt.altKey === true) {
      this.isDragging = true;
      this.selection = false;
      this.lastPosX = evt.clientX;
      this.lastPosY = evt.clientY;
    }
  }

  static pan_mousemove(opts) {

    if (this.isDragging) {
      var e = opts.e;
      var vpt = this.viewportTransform;
      vpt[4] += e.clientX - this.lastPosX;
      vpt[5] += e.clientY - this.lastPosY;
      this.lastPosX = e.clientX;
      this.lastPosY = e.clientY;
      this.requestRenderAll();
    }
  }

  static pan_mouseup(opts) {
    this.setViewportTransform(this.viewportTransform);
    this.isDragging = false;
    this.selection = true;
  }



}