class MM_controller {

  static mzdMainCanvas = null;
  static mzdToolbarCanvas = null;

  static mzdTabviewCanvas = null;

  static zoomViews = [];
  static defineZoomViewMode = false;

  static tabScalingFactor = 0.2



  static init() {

    var containterWidth = document.getElementsByClassName("multiZoomDefinitionMode")[0].offsetWidth - 50; // subtract toolbar width
    var containterHeight = document.getElementsByClassName("multiZoomDefinitionMode")[0].offsetHeight;

    var tabViewDiv = document.getElementsByClassName("tabView")[0]

    var screenHeight = window.outerHeight
    var screenWidth = window.outerWidth

    if (MasterController.mzdMainCanvas === null) {
      tabViewDiv.style.width = screenWidth;
      tabViewDiv.style.height = screenHeight;



      MasterController.addDocumentEventListners()

      MasterController.mzdMainCanvas = new fabric.Canvas('MzdCanvas', {
        selection: false
      });
      MasterController.mzdMainCanvas.backgroundColor = Constants.COLORS.CANVAS_BACKGROUND;
      MasterController.mzdMainCanvas.setHeight(containterHeight);
      MasterController.mzdMainCanvas.setWidth(containterWidth);
      MasterController.mzdMainCanvas.renderAll();
      MasterController.mzdMainCanvas.requestRenderAll();
      MM_controller.mzdMainCanvas = MasterController.mzdMainCanvas

      MasterController.mzdMainCanvas.on('mouse:wheel', MM_viewActionsHandler.zoom_wheel);
      MasterController.mzdMainCanvas.on('mouse:down', MM_viewActionsHandler.pan_mousedown);
      MasterController.mzdMainCanvas.on('mouse:move', MM_viewActionsHandler.pan_mousemove);
      MasterController.mzdMainCanvas.on('mouse:up', MM_viewActionsHandler.pan_mouseup);


      MasterController.mzdMainCanvas.on('mouse:down', MM_drawingActionsHandler.defineZoomView_mousedown);
      MasterController.mzdMainCanvas.on('mouse:move', MM_drawingActionsHandler.defineZoomView_mousemove);
      MasterController.mzdMainCanvas.on('mouse:up', MM_drawingActionsHandler.defineZoomView_mouseup);

      MasterController.mzdMainCanvas.on('object:scaling', MM_drawingActionsHandler.objectScalingHandler_scaled);



      MasterController.mzdMainCanvas.on('object:added', function(e) {

        if (e.target.meta && e.target.meta !== "ignore") {
          console.log(e)
        }
      });

      MasterController.mzdToolbarCanvas = new fabric.Canvas('MzdToolbar');
      MasterController.mzdToolbarCanvas.setHeight(containterHeight);
      MasterController.mzdToolbarCanvas.backgroundColor = Constants.COLORS.TOOLBAR_BACKGROUND;
      MasterController.mzdToolbarCanvas.requestRenderAll();
      MM_controller.mzdToolbarCanvas = MasterController.mzdToolbarCanvas

      MM_toolbarHandler.loadLeftToolbar(this.mzdToolbarCanvas)



      MasterController.mzdTabviewCanvas = new fabric.Canvas('tabViewer', {
        selection: false
      });
      MasterController.mzdTabviewCanvas.setHeight(MM_controller.tabScalingFactor * screenHeight);
      MasterController.mzdTabviewCanvas.setWidth(MM_controller.tabScalingFactor * screenWidth);

      MasterController.mzdTabviewCanvas.backgroundColor = "#fff";
      MasterController.mzdTabviewCanvas.requestRenderAll();

      MasterController.mzdTabviewCanvas.on('object:moved', MM_drawingActionsHandler.childMovementHandler_moved);
      MasterController.mzdTabviewCanvas.on('object:scaling', MM_drawingActionsHandler.childScalingHandler_scaling);


      MM_controller.mzdTabviewCanvas = MasterController.mzdTabviewCanvas

    }



  }



}