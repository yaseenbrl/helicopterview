class MM_drawingActionsHandler {

  static tempRect = null;
  static origX;
  static origY;


  static objectScalingHandler_scaled(opts) {

    if (opts.target && opts.target.meta && opts.target.meta.id.includes('zoom')) {
      opts.target.height *= opts.target.scaleY;
      opts.target.width *= opts.target.scaleX;

      opts.target.scaleX = 1;
      opts.target.scaleY = 1;

      opts.target.child.width = opts.target.width * opts.target.child.magnification
      opts.target.child.height = opts.target.height * opts.target.child.magnification

      opts.target.meta.tabHeight = opts.target.child.height / opts.target.child.canvas.height
      opts.target.meta.tabWidth = opts.target.child.width / opts.target.child.canvas.width

      opts.target.child.canvas.renderAll()
      opts.target.canvas.renderAll()
    }
  }

  static childScalingHandler_scaling(opts) {
    console.log("childScalingHandler_scaling(opts)")

    if (opts.target) {


      var original_height = opts.target.height
      var original_width = opts.target.width

      opts.target.height *= opts.target.scaleY;
      opts.target.width *= opts.target.scaleX;

      opts.target.scaleX = 1;
      opts.target.scaleY = 1;

      if (opts.transform.action == 'scaleY') {
        opts.target.magnification = opts.target.width / opts.target.parent.width
        opts.target.width *= (opts.target.height / original_height)

      } else if (opts.transform.action == 'scaleX') {
        opts.target.magnification = opts.target.height / opts.target.parent.height
        opts.target.height *= (opts.target.width / original_width)
      } else {
        opts.target.magnification = opts.target.width / opts.target.parent.width
      }
      opts.target.parent.meta.magnification = opts.target.magnification

      opts.target.parent.meta.tabHeight = opts.target.height / opts.target.canvas.height
      opts.target.parent.meta.tabWidth = opts.target.width / opts.target.canvas.width









    }
  }

  static childMovementHandler_moved(opts) {

    if (opts.target) {




      opts.target.parent.meta.tabPositionX = opts.target.left / opts.target.canvas.width
      opts.target.parent.meta.tabPositionY = opts.target.top / opts.target.canvas.height

    }
  }



  static defineZoomView_mousemove(opts) {


    if (MM_controller.defineZoomViewMode && opts.e.altKey === false && MM_drawingActionsHandler.tempRect) {

      var pointer = MasterController.getPoint(opts);

      if (MM_drawingActionsHandler.origX > pointer.x) {
        MM_drawingActionsHandler.tempRect.set({
          left: Math.abs(pointer.x)
        });
      }
      if (MM_drawingActionsHandler.origY > pointer.y) {
        MM_drawingActionsHandler.tempRect.set({
          top: Math.abs(pointer.y)
        });
      }

      MM_drawingActionsHandler.tempRect.set({
        width: Math.abs(MM_drawingActionsHandler.origX - pointer.x)
      });
      MM_drawingActionsHandler.tempRect.set({
        height: Math.abs(MM_drawingActionsHandler.origY - pointer.y)
      });

      MasterController.canvasInAction.requestRenderAll();

    }


  }

  static defineZoomView_mouseup(opts) {
    if (MM_controller.defineZoomViewMode && opts.e.altKey === false) {
      if (MM_drawingActionsHandler.tempRect) {
        MM_controller.zoomViews.push(MM_drawingActionsHandler.tempRect);
        var tabViewRepresentative = new fabric.Rect({
          height: MM_drawingActionsHandler.tempRect.height * MM_controller.tabScalingFactor,
          width: MM_drawingActionsHandler.tempRect.width * MM_controller.tabScalingFactor,
          fill: MM_drawingActionsHandler.tempRect.stroke,
          magnification: MM_controller.tabScalingFactor,
          stroke: 'black',
          strokeUniform: true,
          strokeWidth: 2,


          lockScalingFlip: true,
          objectCaching: false,


          lockRotation: true,


          top: 0.5 * MM_drawingActionsHandler.tempRect.height * MM_controller.tabScalingFactor,
          left: 0.5 * MM_drawingActionsHandler.tempRect.width * MM_controller.tabScalingFactor,
          parent: MM_drawingActionsHandler.tempRect
        })
        MM_drawingActionsHandler.tempRect.meta.tabHeight = tabViewRepresentative.height / MM_controller.mzdTabviewCanvas.height
        MM_drawingActionsHandler.tempRect.meta.tabWidth = tabViewRepresentative.width / MM_controller.mzdTabviewCanvas.width
        MM_drawingActionsHandler.tempRect.meta.windowOuterWidth = window.outerWidth
        MM_drawingActionsHandler.tempRect.meta.windowOuterHeight = window.outerHeight

        MM_drawingActionsHandler.tempRect.child = tabViewRepresentative
        MM_controller.mzdTabviewCanvas.add(tabViewRepresentative)
        MM_controller.mzdTabviewCanvas.bringToFront(tabViewRepresentative)
        MM_controller.mzdTabviewCanvas.setActiveObject(tabViewRepresentative)

        MM_drawingActionsHandler.tempRect = null
        MasterController.mzdToolbarCanvas.discardActiveObject().requestRenderAll();
      }
      MM_controller.defineZoomViewMode = false;
      MasterController.canvasInAction.requestRenderAll();
      MM_controller.mzdTabviewCanvas.requestRenderAll();
    }

  }

  static defineZoomView_mousedown(opts) {

    if (MM_controller.defineZoomViewMode && opts.e.altKey === false) {
      var pointer = MasterController.getPoint(opts)
      MM_drawingActionsHandler.origX = pointer.x;
      MM_drawingActionsHandler.origY = pointer.y;
      var pointer = MasterController.getPoint(opts)
      var rect = new fabric.Rect({
        left: MM_drawingActionsHandler.origX,
        top: MM_drawingActionsHandler.origY,
        originX: 'left',
        originY: 'top',
        width: pointer.x - MM_drawingActionsHandler.origX,
        height: pointer.y - MM_drawingActionsHandler.origY,
        angle: 0,
        lockRotation: true,
        objectCaching: false,
        fill: 'rgba(255,255,255,0.33)',
        stroke: Constants.COLORS.zoomViews[MM_controller.zoomViews.length],
        lockScalingFlip: true,
        strokeWidth: 10,
        transparentCorners: false,
        strokeUniform: true,
        meta: {
          id: "zoom_" + (MM_controller.zoomViews.length + 1),
          tabPositionX: 0,
          tabPositionY: 0,
          magnification: MM_controller.tabScalingFactor,
          tabScalingFactor: MM_controller.tabScalingFactor
        }
      });

      MM_drawingActionsHandler.tempRect = rect;

      MasterController.canvasInAction.add(rect);
      MasterController.canvasInAction.requestRenderAll();
    }
  }






}