const jsonbin_url = 'https://api.npoint.io/5e8316827fa55886ccbd'

function save_json(json_data) {
	var xhr = new XMLHttpRequest();
	var url = jsonbin_url;
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status === 200) {
			var json = JSON.parse(xhr.responseText);
		}
	};
	xhr.send(json_data);
}

function load_json() {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", jsonbin_url, false); // false for synchronous request
	xmlHttp.send(null);
	return JSON.parse(xmlHttp.responseText);
}


function upload() {

	json = canvas.toJSON(["ID"])
	save_json(JSON.stringify(json))
	console.log(JSON.stringify(json))

	alert("saved")
}


function download() {


	canvas.clear();
	console.log(load_json())
	canvas.loadFromJSON(load_json(), function(){

		canvas.remove(canvas.item(0))
		canvasInitializeBlueprint(background_image, function(){enableDrawing(blueprint)});

	});

	for(var i =0; i < canvas._objects.length; i++){
			obj = canvas._objects[i];
			obj.ID? label(obj, obj.ID, {top:obj.getCenterPoint().x, left:obj.getCenterPoint().y}) : '';
			console.log(" **** " + i)
		}


	console.log("Loaded ! ")


}







function Copy() {
	// clone what are you copying since you
	// may want copy and paste on different moment.
	// and you do not want the changes happened
	// later to reflect on the copy.
	canvas.getActiveObject().clone(function(cloned) {
		_clipboard = cloned;
	});
	// console.log("COPIED .. !! ")
}

function Paste(sameLocation = false) {
	// console.log("PASTED .. !! ")
	// clone again, so you can do multiple copies.
	_clipboard.clone(function(clonedObj) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: sameLocation? clonedObj.left: clonedObj.left + 10,
			top: sameLocation? clonedObj.top: clonedObj.top + 10,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj) {
				canvas.add(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		_clipboard.top += 10;
		_clipboard.left += 10;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}
