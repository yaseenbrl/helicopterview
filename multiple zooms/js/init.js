window.onload = function() {
	dragElement(document.getElementById("draggableDiv"));
	console.log("his")

}


function init (){
	// prevents right click (bettter for using ctrl key to align drawing point vertically)
	document.addEventListener('contextmenu', event => event.preventDefault());
	document.addEventListener('keyup', onkeyup);
	document.addEventListener('keydown', onkeydown);


	dragElement(document.getElementById("draggableDiv"));

	canvas = new fabric.Canvas('c');
	canvas.backgroundColor = COLORS.BACKGROUND;
	var shadow = new fabric.Shadow({
            color: 'red',
            blur: 50
        });
	canvasChangeCursor("default")
	canvas.requestRenderAll();
	canvas.on('mouse:wheel', canvasWheelHandler);
	canvas.on('mouse:down', canvasMouseDownHandler);
	canvas.on('mouse:move', canvasMouseMoveHandler);
	canvas.on('mouse:up', canvasMouseUpHandler);
	canvas.on('mouse:over', canvasMouseOverHandler);
	canvas.on('mouse:out', canvasMouseOutHandler);
	// initializes blueprint.
	canvasInitializeBlueprint(background_image, function(){enableDrawing(blueprint); });
	loadLeftToolbar();
	// toggleMode({checked:document.getElementById("toggleMode").checked})

}
