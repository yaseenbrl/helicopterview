// fabric.labeledRect = fabric.util.createClass(fabric.Rect, {

//   type: 'labeledRect',
//   // initialize can be of type function(options) or function(property, options), like for text.
//   // no other signatures allowed.
//   initialize: function(options) {
//     options || (options = { });

//     this.callSuper('initialize', options);
//     this.set('label', options.label || '');
//   },

//   toObject: function() {
//     return fabric.util.object.extend(this.callSuper('toObject'), {
//       label: this.get('label')
//     });
//   },

//   _render: function(ctx) {
//     this.callSuper('_render', ctx);

//     ctx.font = '20px Helvetica';
//     ctx.fillStyle = '#333';
//     ctx.fillText(this.label, -this.width/2, -this.height/2 + 20);
//   }
// });


// // standard options type:
// fabric.labeledRect.fromObject = function(object, callback) {
//   return fabric.Object.fromObject('labeledRect', object, callback);
// }

// // argument + options type:
// // in this example aProp is the property in the object that contains the value
// // that goes in someValue in `new fabric.MyClass(someValue, options)`
// // fabric.labeledRect.fromObject = function(object, callback) {
// //   return fabric.Object._fromObject('LabeledRect', object, callback, 'aProp');
// // }