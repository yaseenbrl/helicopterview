function canvasChangeCursor(new_cursor) {
	if (new_cursor != cursorsHistory[cursorsHistory.length - 1]);
	cursorsHistory.push(new_cursor);
	canvas.hoverCursor = new_cursor;
	canvas.requestRenderAll();
}



function canvasShowBlueprint(cb) {
	// alert("Clicked, new value = " + cb.checked);
	all_objects = canvas._objects;
	if(cb.checked){
		
		for(var i =0; i < all_objects.length; i++){
				all_objects[i].opacity = 0.5;
		}
		blueprint.opacity = 1;

	} else{
		
		for(var i =0; i < all_objects.length; i++){
				all_objects[i].opacity = 1;
		}
		blueprint.opacity = 0;
	}
	canvas.requestRenderAll();
}



function canvasInitializeBlueprint(_background_image, _callback) {

	fabric.Image.fromURL(_background_image, function(oImg) {
		oImg.set({
			left: 0,
			top: 0,
			left: 0,
			angle: 0,
			opacity: 1,
			stroke: '#888',
			strokeWidth: 2,
			selectable: false
		})
		canvas.add(oImg);
		canvas.sendToBack(oImg);
		blueprint = oImg;
		if (blueprint != null) _callback(blueprint);
	});

}


