function updateDetailsBox(html) {
	document.getElementById('details').innerHTML = html
}



function getLetters(text) {
	return text.replace(/[^A-Za-z]/g, '');
}

function getNumbers(text) {
	return text.replace(/[^0-9]/g, '');
}


function generateIDs(start_id, end_id) {
	if (/([a-zA-Z]*[0-9]+)/.test(start_id) == false) {
		alert("Can't populate this pattern");
		return null;
	}
	length = start_id.length
	letters = getLetters(start_id);
	if (letters !== getLetters(end_id)) {
		alert("Can't populate this pattern");
		return null;
	}
	start_number = parseInt(getNumbers(start_id))
	end_number = parseInt(getNumbers(end_id))
	var IDs = new Array();
	for (var i = start_number; i <= end_number; i++) {
		zeros_length = length - (i).toString().length - letters.length
		IDs.push(letters + "0".repeat(zeros_length) + i)
	}
	console.log("Generated: " + IDs)
	return IDs

}


function defineID(object, text) {
	// console.log("at label:")
	// console.log(object)
	// console.log("left:")
	// console.log(location.left)
	// object.setCoords();
	// textObj = new fabric.Text(text,{
	// 	fill:'white',
	// 	fontSize:18,
	// 	left:location.left,
	// 	top:location.top,
	// 	originX:'center',
	// 	originY:'center',
	// })
	// canvas.add(textObj);
	object.fill = '#0f0'
	object.ID = text;
}
// -7.142857142857143*canvas.getZoom()+29.285714285714285
function label(_canvas, object, text, fontSize=5) {
	// console.log("at label:")
	// console.log(object)
	// console.log("left:")
	// console.log(location.left)
	// console.log("Labeling: " + text)
	// if (text === "IU03"){console.log("*")}
	object.setCoords();
	index = canvas._objects.indexOf(object);
	textObj = new fabric.Text(text, {
		fill: 'black', //invertColor(object.fill),
		fontSize: fontSize,
		fontWeight: '100',
		left: object.getCenterPoint().x,
		top: object.getCenterPoint().y,
		originX: 'center',
		originY: 'center',
		angle: (object.angle - 90) < -90 ? 180+(object.angle - 90) : object.angle - 90,
		opacity: 1,
		evented: false,
		selectable: false,
	})

	// var group  = new fabric.Group([object, textObj]);
	// group.ID = object.ID;
	// canvas._objects[index] = group;
	// canvas.add(group);
	_canvas.add(textObj);

}


function invertColor(hex, bw = false) {
	function padZero(str, len) {
		len = len || 2;
		var zeros = new Array(len).join('0');
		return (zeros + str).slice(-len);
	}
	if (hex.indexOf('#') === 0) {
		hex = hex.slice(1);
	}
	// convert 3-digit hex to 6-digits.
	if (hex.length === 3) {
		hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
	}
	if (hex.length !== 6) {
		throw new Error('Invalid HEX color.');
	}
	var r = parseInt(hex.slice(0, 2), 16),
		g = parseInt(hex.slice(2, 4), 16),
		b = parseInt(hex.slice(4, 6), 16);
	if (bw) {
		// http://stackoverflow.com/a/3943023/112731
		return (r * 0.299 + g * 0.587 + b * 0.114) > 186 ?
			'#000000' :
			'#FFFFFF';
	}
	// invert color components
	r = (255 - r).toString(16);
	g = (255 - g).toString(16);
	b = (255 - b).toString(16);
	// pad each with zeros and return
	return "#" + padZero(r) + padZero(g) + padZero(b);
}


function populateIDs() {
	var activeObject = canvas.getActiveObject();
	var center_point = activeObject.getCenterPoint();
	if (activeObject.type === "activeSelection") {
		objects = activeObject._objects;

		start_id = prompt("What's the first ID?");
		end_id = prompt("What's the last ID?");

		IDs = generateIDs(start_id, end_id);
		if (IDs === null || IDs.length !== objects.length) {
			alert("Generated IDs number doesn't match the number of objects selected")
		}

		for (var i = 0; i < objects.length; i++) {
			console.log(IDs[i]);
			console.log("before label:")
			var top = (objects[i].top + center_point.y) + objects[i].height / 2;
			var left = (objects[i].left + center_point.x) + objects[i].width / 2;
			// console.log({top:top, left:left})
			defineID(objects[i], IDs[i]);
			console.log(objects[i])

		}
	} else {
		var top = center_point.y;
		var left = center_point.x;
		defineID(activeObject, prompt("Insert the object's ID"));
	}

	canvas.discardActiveObject();
	canvas.requestRenderAll();

}



function populateInfo(json, shrink=false) {
	popCount = 0;
	all_objects = canvas._objects;

	for (var i = 0; i < all_objects.length; i++) {
		var obj = all_objects[i];
		var _ID = obj.ID;
		var _top = obj.top + obj.height / 2;
		var _left = obj.left + obj.width / 2;
		// if (obj.ignore){
		// 	canvas.remove(obj)
		// 	continue
		// }
		obj.set({
			selectable: false,
			width: shrink ? obj.width-2: obj.width,
			height: shrink ? obj.height-2: obj.height,
			strokeWidth:0.5,
		})

		if (_ID) {

			obj.set({
					// shadow:{color: 'rgba(0,0,0,0.3)', offsetX:2,offsetY:0, blur:2},
					opacity:1,

				});
			if (_ID in json) {

				var unit_fill_color =   COLORS.STATIC;
				if (json[_ID].unit !== null){
					unit_fill_color = (json[_ID].unit.status === 'Loading') ? COLORS.UNIT_LOADING :  COLORS.UNIT_UNLOADING;
				}
				var unit = new fabric.Rect({
					originX:'center',
					originY:'top',
					top:(obj.aCoords.tl.y + obj.aCoords.tr.y)/2,
					left:(obj.aCoords.tl.x + obj.aCoords.tr.x)/2,
					width:0.75*obj.width,
					height:0.75*(obj.height)/5,
					fill: unit_fill_color,
					angle:obj.angle,
					opacity: json[_ID].unit ? 1: 0,
					ignore:true,
				})
				canvas.add(unit);

				obj.set({
					info: json[_ID],
					status: json[_ID].status,
					// unitNum: json[_ID].unit ? json[_ID].unit.num : null,
				});

				if (obj.status === 'Available') {
					obj.set({
						fill: COLORS.AVAILABLE
					});
				} else if (obj.status === 'Closed') {
					obj.set({
						fill: COLORS.CLOSED
					});
				} else if (obj.status === 'Occupied') {
					obj.set({
						fill: COLORS.OCCUPIED
					});
				} else if (obj.status === 'Booked') {
					obj.set({
						fill: COLORS.BOOKED
					});
				}
				if (obj.ID === 0) {
					obj.set({
						fill: COLORS.STATIC,
						shadow:{color: 'rgba(0,0,0,0.35)', offsetX:3,offsetY:0, blur:5}
					});

				}
				popCount++;
			} else {
				// obj.set({ID : 0});

				obj.set({
					status: null
				});
				obj.set({
					unitNum: null
				});
				obj.set({
					fill: COLORS.UNAVAILABLE
				});
				obj.set({
					stroke: COLORS.BORDER_DARK,
				});

			}
			// if (obj.ID !== 0){
			// label(obj, _ID, {
			// 	top: _top,
			// 	left: _left
			// })}
		} else {
			if (obj.type !== 'text' && !obj.ignore) {
				obj.set({
					status: null
				});
				obj.set({
					unitNum: null
				});
				obj.set({
					fill: COLORS.STATIC
				});
				obj.set({
					stroke: COLORS.BORDER_DARK,
				});
			}
		}

	}


	viewRect.set({
			fill:'rgba(0,0,0,0)',
			opacity:1,
			 strokeWidth: viewRect.original_strokeWidth/canvas.getZoom(),
			 evented:false,
			 selectable:false,
			});
	canvas.requestRenderAll()
	// refreshZoomCanvas(null, reload=true)
		// alert("Populated " + popCount + "objects")
}



function fetchInfo() {

	enableInteractiveMode();
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", 'https://api.npoint.io/2666c13c5b92af542125', false); // false for synchronous request
	xmlHttp.send(null);
	populateInfo(JSON.parse(xmlHttp.responseText));
	canvas.requestRenderAll();
	info = JSON.parse(xmlHttp.responseText);
	canvas.remove(canvas.item(0)) //remove background

}


function enableInteractiveMode() {
	interactiveMode = true;
	canvas.selectionColor = 'rgba(0,255,0,0)';
	canvas.selectionBorderColor = 'rgba(0,255,0,0)';
	canvas.selectionLineWidth = 0;

}



function refreshZoomCanvas(opt, reload=true){

	if(opt == null){
		opt = cache_opt
	} else {
		cache_opt = opt
	}

	if(reload){
		canvas.requestRenderAll()
    	canvasZ.loadFromJSON(canvas.toJSON(["info", "ID"]));
	 	canvasZ.remove(canvasZ.item(canvasZ._objects.length-1))

	for (var i = 0; i < canvasZ._objects.length; i++) {
		var obj = canvasZ._objects[i];
	 	if (obj.ID && obj.ID !== '0'){
			label(canvasZ,obj, obj.ID, {
				top: obj.top + obj.height / 2,
				left: obj.left + obj.width / 2
				})}
		}
		// channel.postMessage(canvas.toJSON(["info", "ID"]))
	 	canvasZ.requestRenderAll();
	}



    var vptZ = canvasZ.viewportTransform;
    var vptO = canvas.viewportTransform;
    vptZ[4] = -(opt.e.clientX - canvas._offset.left) * vptZ[0]/vptO[0] + 0.5 * canvasZ.width + vptO[4]*vptZ[0]/vptO[0];
    vptZ[5] = -(opt.e.clientY - canvas._offset.top) * vptZ[0]/vptO[0] + 0.5 * canvasZ.height + vptO[5]*vptZ[0]/vptO[0];

    pointerRect.set({
	top: (canvasZ.getCenter().top-vptZ[5])/ vptZ[0],
	left: (canvasZ.getCenter().left-vptZ[4])/ vptZ[0],
	width: pointerRect.original_width/vptZ[0],
	height: pointerRect.original_height/vptZ[0],
	strokeWidth: pointerRect.original_strokeWidth/vptZ[0]
	});
	canvasZ.bringToFront(pointerRect);
	// console.log(pointerRect)
	// canvasZ.requestRenderAll();

}

function toggleMode(cb){
	if (!cb.checked){
		document.getElementById("drawMode").style.display = 'none'
		document.getElementById("interactiveMode").style.display = 'flex'
		document.addEventListener('keydown', showStatus);
		document.addEventListener('keydown', showUnitInfo);
		document.addEventListener('keydown', showUnitNum);
		document.addEventListener('keyup', showID);



		canvas = new fabric.Canvas('origin_canvas');


		canvasZ = new fabric.Canvas('zoom_canvas');

		canvas.backgroundColor = COLORS.BACKGROUND;
		canvasZ.backgroundColor = COLORS.BACKGROUND;

		download();

		viewRect = new fabric.Rect({
			fill:'rgba(0,0,0,0)',
			opacity:1, stroke:'red',
			 strokeWidth:4,
			 evented:false,
			 selectable:false,

			});
		viewRect.original_strokeWidth=viewRect.strokeWidth
		canvas.add(viewRect);

		// canvasZ.setOverlayImage('https://cdn.icon-icons.com/icons2/1464/PNG/32/cross_100185.png')
		pointerRect = new fabric.Rect({
			 fill:'rgb(255,255,255)',
			 opacity:1, stroke:'black',
			 strokeWidth: 2,
			 evented:false,
			 selectable:false,
			 height:5,
			 width:5,
			 top: canvasZ.getCenter().top-canvasZ.viewportTransform[5],
			 left: canvasZ.getCenter().left-canvasZ.viewportTransform[4],
			});
		pointerRect.original_width=pointerRect.width
		pointerRect.original_height=pointerRect.height
		pointerRect.original_strokeWidth=pointerRect.strokeWidth
		canvasZ.add(pointerRect);

		document.getElementById("origin_canvas_wrapper").addEventListener("mouseup", function(){
			canvas.setWidth(this.offsetWidth - 50 );
			canvas.setHeight( this.offsetHeight - 50 );
			canvas.calcOffset();
		});




		document.getElementById("zoom_canvas_wrapper").addEventListener("mouseup", function(){
			canvasZ.setWidth(this.offsetWidth - 50 );
			canvasZ.setHeight( this.offsetHeight - 50 );
			canvasZ.calcOffset();
		});



		// canvasChangeCursor("default")

		canvasZ.requestRenderAll();
		canvas.on('mouse:wheel', canvasWheelHandler);
		canvas.on('mouse:down', canvasMouseDownHandler);
		canvas.on('mouse:move', canvasMouseMoveHandler);
		canvas.on('mouse:up', canvasMouseUpHandler);
		canvas.on('mouse:over', canvasMouseOverHandler);
		canvas.on('mouse:out', canvasMouseOutHandler);
		interactiveMode = true;
		canvas.selectionColor = 'rgba(0,0,0,0)';
		canvas.selectionBorderColor = 'rgba(0,0,0,0)';
		canvas.selectionLineWidth = 0;





		canvas.requestRenderAll()
    	canvasZ.loadFromJSON(canvas.toJSON(["info", "ID"]));
	 	canvasZ.remove(canvasZ.item(canvasZ._objects.length-1))

	 	canvasZ.requestRenderAll();



	} else{
		interactiveMode = false;
		document.getElementById("drawMode").style.display = 'grid'
		document.getElementById("interactiveMode").style.display = 'none'
		init ();
		canvasZ = null;
	}

}
