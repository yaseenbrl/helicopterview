var channel = new BroadcastChannel("zoomChannel");


var canvas = null;
var blueprint = null;
var drawPolygonMode = false;
var drawTileMode = false;
var toolInProcess = null;
var interactiveMode = false;


var background_image = './assets/blueprints/large.png';
var cursorsHistory = [];


// used in events handlers
var ctrlDown = false;
var ctrlKey = 17;
var cmdKey = 93;
var vKey = 86;
var cKey = 67;
var delKey = 8;
var backspaceKey = 46;



// used for the polygon/tile drawing
var polygonPoints = [];
var finalPolygon = null;
var pointers = [];


// toolbar
var toolbar_canvas = null;
var leftToolbar = null;



// copy paste
var _clipboard = null;


const COLORS = {
	STATIC: '#445055',
	AVAILABLE: '#ffffff',
	OCCUPIED: '#dfeaf6',
	CLOSED: '#efa194',
	BOOKED: '#dbdbdb',
	BACKGROUND: '#bdb19f',
	BORDER_DARK: '#57423F',
	UNAVAILABLE: 'd2d3d4',
	UNIT_UNLOADING: '#693b97',
	UNIT_LOADING: '#f6c05a',
}


// info
var info = null


// drag drop

var drag_starting_ID = null;
var dragline_start_point = null;
var dragline = null;


var canvasZ_cache = null;
var cache_opt = null
