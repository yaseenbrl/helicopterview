canvasWheelHandler = function(opt) {

    if (opt.e.altKey === true) {
        var delta = opt.e.deltaY;
        var zoom = canvas.getZoom();
        zoom *= 0.999 ** delta;
        if (zoom > 20) zoom = 20;
        if (zoom < 0.01) zoom = 0.01;
        canvas.setZoom(zoom);

        opt.e.preventDefault();
        opt.e.stopPropagation();
    }
    if (interactiveMode) {
        var delta = opt.e.deltaY;
        var zoom = canvasZ.getZoom();
        zoom *= 0.999 ** delta;
        if (zoom > 20) zoom = 20;
        if (zoom < 0.01) zoom = 0.01;
        canvasZ.setZoom(zoom);
        opt.e.preventDefault();
        opt.e.stopPropagation();
        viewRect.set({
            width: canvasZ.width / canvasZ.getZoom(),
            height: canvasZ.height / canvasZ.getZoom(),
            left: canvasZ.viewportTransform[4] / -canvasZ.getZoom(),
            top: canvasZ.viewportTransform[5] / -canvasZ.getZoom(),
            strokeWidth: viewRect.original_strokeWidth/canvas.getZoom(),
        });
        refreshZoomCanvas(opt, reload = false)
    }
    canvas.requestRenderAll()

}


canvasMouseDownHandler = function(opt) {
    var evt = opt.e;
    if (evt.altKey === true) {
        this.isDragging = true;
        this.selection = false;
        this.lastPosX = evt.clientX;
        this.lastPosY = evt.clientY;
    }
    if (interactiveMode == true) {
        if (opt.target && opt.target.ID && opt.target.ID !== "0") {
            dragline_start_point = getPoint(opt);
            drag_starting_ID = opt.target.ID;

        }

        refreshZoomCanvas(opt)
    }

}


canvasMouseMoveHandler = function(opt) {
    if (this.isDragging) {
        var e = opt.e;
        var vpt = this.viewportTransform;
        vpt[4] += e.clientX - this.lastPosX;
        vpt[5] += e.clientY - this.lastPosY;
        this.requestRenderAll();
        this.lastPosX = e.clientX;
        this.lastPosY = e.clientY;
        // console.log(e.clientX +"    " + e.clientY)
    }


    if (interactiveMode) {

        if (drag_starting_ID !== null) {
            new_point = getPoint(opt);

            if (dragline) {
                canvas.remove(dragline);
            }
            dragline = new fabric.Line([new_point.x, new_point.y, dragline_start_point.x, dragline_start_point.y], {
                fill: 'red',
                strokeWidth: 2,
                stroke: 'red',
                evented: false,
                selectable: false
            });
            canvas.add(dragline);
            canvas.requestRenderAll();
        }



        refreshZoomCanvas(opt, reload = false)

        viewRect.set({
            width: canvasZ.width / canvasZ.getZoom(),
            height: canvasZ.height / canvasZ.getZoom(),
            left: canvasZ.viewportTransform[4] / -canvasZ.getZoom(),
            top: canvasZ.viewportTransform[5] / -canvasZ.getZoom(),
            strokeWidth: viewRect.original_strokeWidth/canvas.getZoom(),
        })

        canvas.bringToFront(viewRect)
        canvas.requestRenderAll()

    }

}

canvasMouseUpHandler = function(opt) {

    this.setViewportTransform(this.viewportTransform);
    this.isDragging = false;
    this.selection = true;
    if (interactiveMode) {
        if (drag_starting_ID && opt.target && opt.target.ID && (opt.target.ID !== "0") && (drag_starting_ID !== opt.target.ID)) {
            if (info[drag_starting_ID].unitNum) {
                alert("Move unit " + getNumbers(info[drag_starting_ID].unitNum) + " from " + drag_starting_ID + " to " + opt.target.ID)
            } else {
                alert("Drag from " + drag_starting_ID + " to " + opt.target.ID)
            }

            drag_starting_ID = null;

        }
        if (dragline) {
            canvas.remove(dragline);
            dragline = null;
            drag_starting_ID = null;
            dragline_start_point = null;

        }
        refreshZoomCanvas(opt, reload = true)
    }

}


//

canvasMouseOutHandler = function(e) {
    // e.target.set({scale: 1});
    if (e.target !== null && e.target.type !== "image" && e.target.ID !== undefined && e.target.ID !== "0") {
        //   e.target.set({
        //   	originX : e.target.original_originX,
        //   	originY : e.target.original_originY,
        //   	top: e.target.original_top,
        // left: e.target.original_left,

        //   });
        //   e.target.scale(1);

        updateDetailsBox("")
        obj_index = canvas._objects.indexOf(e.target)
        canvasZ.item(obj_index).set({
            stroke: canvasZ.item(obj_index).original_stroke,
        });
        canvasZ.requestRenderAll();

        if (interactiveMode) refreshZoomCanvas(e, reload = false)
    }
    canvasZ_cache = null
}

canvasMouseOverHandler = function(e) {
    if (e.target !== null && e.target.type !== "image" && e.target.ID !== undefined && e.target.ID !== "0") {
        // canvas.bringToFront(e.target)
        //    e.target.set({
        //    	original_originX: e.target.originX,
        //    	original_originY: e.target.originY,
        // 	original_top : e.target.top,
        // 	original_left : e.target.left,
        //    });
        //    var center = e.target.getCenterPoint()
        //    e.target.set({
        //    	originX: 'center',
        //    	originY: 'center',
        //    	left: center.x,
        //        top: center.y
        //    });
        //    e.target.scale(1.2)

        obj_index = canvas._objects.indexOf(e.target)
        canvasZ.item(obj_index).set({
            original_stroke: canvasZ.item(obj_index).stroke
        });
        canvasZ.item(obj_index).set({
            stroke: '#fff'
        })
        canvasZ_cache = canvasZ.item(obj_index)

        if (info && e.target.ID) {
            if (e.target.ID in info) {
                html = "<p> ID : " + e.target.ID + "</p>" + "<p> Status : " + info[e.target.ID].status + "</p>"
                info[e.target.ID].unitNum ? html += "<p> Unit : " + info[e.target.ID].unitNum + "</p>" : '';
                updateDetailsBox(html)
            }
        };

        if (interactiveMode) refreshZoomCanvas(e, reload = false)
        canvasZ.requestRenderAll();
    }
    // console.log("over")
}
