drawPolygonOn = function() {
	drawPolygonMode = true;
	drawTileMode = false;
	canvasChangeCursor("crosshair");
	console.log("Draw polygon");

}


drawRectangle = function() {
	rect = new fabric.Rect({
		width: 100,
		height: 100,
		left: canvas.getCenter().left,
		top: canvas.getCenter().top,
		opacity: document.getElementById("opacity").value / 100,
		fill: document.getElementById("fillColorPicker").value,
		stroke: document.getElementById("borderColorPicker").value,
		strokeWidth: document.getElementById("hasBorders").checked ? 2 : 0,
		selectable: true,
	});

	canvas.add(rect);
	toolbar_canvas.discardActiveObject();
	toolbar_canvas.requestRenderAll();
}

drawTile = function() {
	drawPolygonMode = true;
	drawTileMode = true;
	canvasChangeCursor("crosshair");
	console.log("Draw polygon");
}


function addTool(image, toolBar, selectionHandler, vertical = true, order = 1, width = 32, height = 32) {

	fabric.Image.fromURL(image, function(oImg) {
		oImg.set({
			left: (vertical ? toolBar.left + toolBar.width / 2 : toolBar.left + 32 * order),
			top: (vertical ? toolBar.top + 32 * order : toolBar.top + toolBar.height / 2),
			width: width,
			height: height,
			originX: 'center',
			originY: 'center',
			angle: 0,
			opacity: 1,
			selectable: true,
			stroke: '#000',
			strokeWidth: 2,
			shadow: {
				color: "#000000",
				blur: 10,
				offsetX: 1,
				offsetY: 1,
				opacity: 0.5
			},
			borderColor: '#464646',
			borderScaleFactor: 1,
			lockMovementX: true,
			lockMovementY: true,
			hasControls: false,
		})

		toolbar_canvas.add(oImg);
		oImg.on('selected', function() {
			selectionHandler();
			toolbar_canvas.getActiveObject().set({
				stroke: '#f00',
				strokeWidth: 2,
			})
		});

		oImg.on('deselected', function() {
			oImg.set({
				stroke: '#000',
				strokeWidth: 2,
			})
		});


	});

	toolbar_canvas.requestRenderAll();
}



loadLeftToolbar = function() {

	toolbar_canvas = new fabric.Canvas('toolBar');
	toolbar_canvas.backgroundColor = '#242424';
	leftToolbar = new fabric.Rect({
		width: 40,
		height: toolbar_canvas.height,
		left: 0,
		top: 0,
		fill: '#242424',
		// borderColor: '#000',
		selectable: false,
		shadow: {
			color: "#000000",
			blur: 10,
			offsetX: 1,
			offsetY: 1,
			opacity: 0.5
		}
	});

	toolbar_canvas.add(leftToolbar);



	// draw_rectangle_tool = addTool('./assets/icons/draw_rect.png', leftToolbar, drawRectangle, vertical = true, order = 1)
	draw_tile_tool = addTool('./assets/icons/draw_tile.png', leftToolbar, drawTile, vertical = true, order = 2)
	draw_polygon_tool = addTool('./assets/icons/draw_polygon.png', leftToolbar, drawPolygonOn, vertical = true, order = 3)
	upload_tool = addTool('./assets/icons/upload.png', leftToolbar, upload, vertical = true, order = 15)
	download_tool = addTool('./assets/icons/download.png', leftToolbar, download, vertical = true, order = 16)



}