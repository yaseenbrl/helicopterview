function enableDrawing(object) {

  object.on('mousemove', function(opt) {


    if (drawPolygonMode) {
      var new_point = getPoint(opt);
      if (canvas._objects.includes(finalPolygon)) {
        canvas.remove(finalPolygon)
      }

      if (polygonPoints.length >= 1) {
        if (opt.e.ctrlKey === true) {
          new_point.x = polygonPoints[polygonPoints.length - 1].x
        } else if (opt.e.shiftKey === true) {
          new_point.y = polygonPoints[polygonPoints.length - 1].y
        }
      }

      canvas.remove(pointers[pointers.length - 1]);
      pointers.splice(-1);
      pointers.push(drawPointer(new_point));

      if (polygonPoints.length >= 2) {
        finalPolygon = getPolygon([polygonPoints, new_point].flat());
        canvas.add(finalPolygon);
        canvas.requestRenderAll();
      }
    }

  });

  object.on('mousedblclick', function(opt) {


    if (drawPolygonMode) {
      drawPolygonMode = false;
      canvas.hoverCursor = "default";
      finalPolygon.set({
        opacity: document.getElementById("opacity").value / 100,
        fill: document.getElementById("fillColorPicker").value,
        stroke: document.getElementById("borderColorPicker").value,
        strokeWidth: document.getElementById("hasBorders").checked ? 2 : 0,
        selectable: true,
        evented: true,
      })


      canvas.setActiveObject(finalPolygon);



      Copy();
      canvas.remove(finalPolygon);
      Paste(sameLocation = true);


      toolbar_canvas.discardActiveObject().requestRenderAll();

      polygonPoints = [];
      pointers.forEach(a => {
        canvas.remove(a);
      });
      pointers = [];
    }



  })


  object.on('mousedown', function(opt) {
    if (drawPolygonMode && opt.e.altKey === false) {
      new_point = getPoint(opt);

      if (polygonPoints.length >= 1) {
        if (opt.e.ctrlKey === true) {
          new_point.x = polygonPoints[polygonPoints.length - 1].x
        } else if (opt.e.shiftKey === true) {
          new_point.y = polygonPoints[polygonPoints.length - 1].y
        }

      }

      polygonPoints.push(new_point);
      pointers.push(drawPointer(new_point));
    }

    if (polygonPoints.length == 3 && drawTileMode) {
      drawAreaSegmants(polygonPoints[0].x, polygonPoints[0].y,
        polygonPoints[1].x, polygonPoints[1].y,
        polygonPoints[2].x, polygonPoints[2].y,
        parseInt(prompt("How many areas?")));

      polygonPoints = [];
      pointers.forEach(a => {
        canvas.remove(a);
      });
      pointers = [];
      toolbar_canvas.discardActiveObject().requestRenderAll();
      drawPolygonMode = false;
      drawTileMode = false;
      canvas.remove(finalPolygon)
      finalPolygon = null;
      canvasChangeCursor("default")

    }



  })

}



function Point(x, y) {
  this.x = x;
  this.y = y;
}


function getPolygon(points) {
  return new fabric.Polygon(points, {
    strokeWidth: 3,
    fill: 'red',
    selectable: false,
    evented: false,
    opacity: 0.5,
  });
}

function drawPointer(point) {
  pointer = new fabric.Rect({
    width: 15,
    height: 15,
    left: point.x,
    top: point.y,
    opacity: 1,
    fill: document.getElementById("fillColorPicker").value,
    stroke: document.getElementById("borderColorPicker").value,
    strokeWidth: document.getElementById("hasBorders").checked ? 2 : 0,
    selectable: false,
    evented: false,
    originX: 'center',
    originY: 'center',
  })
  canvas.add(pointer);

  return pointer;
}



// gets accurate coordinates considering the zoom/pan 
function getPoint(options) {


  var offset = canvas._offset;
  x = (options.e.pageX - canvas.viewportTransform[4] - offset.left) * (1 / canvas.viewportTransform[0]);
  y = (options.e.pageY - canvas.viewportTransform[5] - offset.top) * (1 / canvas.viewportTransform[3]);


  return new Point(x, y);
}



function getPolygonFrom3Points(Xa, Ya, Xb, Yb, Xc, Yc) {

  console.log("getParallelPoints(" + Xa + "," + Ya + "," + Xb + "," + Yb + "," + Xc + "," + Yc + ")")

  A = Yb - Ya;

  B = Xa - Xb;

  C = Xb * Ya - Xa * Yb;

  C_ = Yc * (Xb - Xa) + Xc * (Ya - Yb);

  Xa_ = (B * B * Xa - A * (B * Ya + C_)) / (A * A + B * B);

  Ya_ = (A * A * Ya - B * (A * Xa + C_)) / (A * A + B * B);


  Xb_ = (B * B * Xb - A * (B * Yb + C_)) / (A * A + B * B);

  Yb_ = (A * A * Yb - B * (A * Xb + C_)) / (A * A + B * B);



  ux = (Xa_ - Xb_) / Math.sqrt(Math.pow(Xa_ - Xb_, 2) + Math.pow(Ya_ - Yb_, 2));
  uy = (Ya_ - Yb_) / Math.sqrt(Math.pow(Xa_ - Xb_, 2) + Math.pow(Ya_ - Yb_, 2));
  d = Math.sqrt(Math.pow(Xa - Xb, 2) + Math.pow(Ya - Yb, 2))

  coords = [
    [Xa, Ya],
    [Xc + (d * ux), Yc + (d * uy)],
    [Xc, Yc],
    [Xb, Yb]
  ]
  return coords;
}


function getEquidistantPoints(Xs, Ys, Xe, Ye, numPoints) {

  // console.log("getEquidistantPoints("+Xs+","+ Ys+","+ Xe+","+ Ye+","+ numPoints+")")

  var dx = Xe - Xs;
  var dy = Ye - Ys;

  // var numPoints = Math.floor(Math.sqrt(dx * dx + dy * dy) /  minDist) - 1;

  var result = new Array;

  var stepx = dx / numPoints;
  var stepy = dy / numPoints;
  var px = Xs + stepx;
  var py = Ys + stepy;
  for (let ix = 0; ix < numPoints; ix++) {
    result.push([px, py]);
    px += stepx;
    py += stepy;
  }

  return result;
}

function getEdgeLength(edge) {
  return Math.sqrt(Math.pow(edge[0][0] - edge[1][0], 2) + Math.pow(edge[0][1] - edge[1][1], 2))
}


function getAngle(edge) {
  return Math.atan((edge[0][0] - edge[1][0]) / (edge[0][1] - edge[1][1]));
}


function getSortedKeys(obj) {
  var keys = Object.keys(obj);
  return keys.sort(function(a, b) {
    return obj[b] - obj[a]
  });
}

function getLengthWidthEdges(edges) {


  lengthEdges = new Array()
  widthEdges = new Array()
  edges_details = {
    0: getEdgeLength(edges[0]),
    1: getEdgeLength(edges[1]),
    2: getEdgeLength(edges[2]),
    3: getEdgeLength(edges[3]),
  }
  sorted_keys = getSortedKeys(edges_details)

  //  console.log(edges_details)
  // console.log(sorted_keys)

  lengths = [
    edges[parseInt(sorted_keys[0])],
    edges[parseInt(sorted_keys[1])]
  ]
  widths = [
    edges[parseInt(sorted_keys[2])],
    edges[parseInt(sorted_keys[3])]
  ]

  return [lengths, widths]

}

function drawAreaSegmants(Xa, Ya, Xb, Yb, Xc, Yc, numAreas) {

  equidistantPointsA = getEquidistantPoints(Xa, Ya, Xb, Yb, numAreas)

  var [
    [_, _],
    [Xd, Yd],
    [Xc, Yc],
    [_, _]
  ] = getPolygonFrom3Points(Xa, Ya, Xb, Yb, Xc, Yc);
  equidistantPointsB = getEquidistantPoints(Xd, Yd, Xc, Yc, numAreas)



  var [xa, ya] = [Xa, Ya];
  var [xd, yd] = [Xd, Yd];

  for (let i = 0; i < equidistantPointsA.length; i++) {

    xc = equidistantPointsB[i][0];
    yc = equidistantPointsB[i][1];
    xb = equidistantPointsA[i][0];
    yb = equidistantPointsA[i][1];
    coords = [
      new Point(xa, ya),
      new Point(xd, yd),
      new Point(xc, yc),
      new Point(xb, yb),
    ];


    

    var [[l1, l2],[w1, w2]] = getLengthWidthEdges([
                                              [[xa, ya], [xd, yd]],
                                              [[xd, yd], [xc, yc]],
                                              [[xc, yc], [xb, yb]],
                                              [[xb, yb], [xa, ya]],
                                            ]);

    // console.log("l1 : ")
    // console.log(l1)
    // console.log("l2 : ")
    // console.log(l2)
    // console.log("w1 : ")
    // console.log(w1)
    // console.log("w2 : ")
    // console.log(w2)



    // L.polygon(coords, {draggable:true}).addTo(map);
    poly = new fabric.Polygon(coords, {
      fill: document.getElementById("fillColorPicker").value,
      stroke: document.getElementById("borderColorPicker").value,
      strokeWidth: document.getElementById("hasBorders").checked ? 2 : 0,
      opacity: document.getElementById("opacity").value / 100,
      originY:'center',
      originX: 'center',
    });

    // console.log({
    //   top: poly.top,
    //   left: poly.left,
    //   width: getEdgeLength(w1),
    //   height: getEdgeLength(l1),
    //   angle: getAngle(l1) * (180 / Math.PI),
    //   fill: 'red',
    //   stroke: document.getElementById("borderColorPicker").value,
    //   strokeWidth: document.getElementById("hasBorders").checked ? 2 : 0,
    //   opacity: document.getElementById("opacity").value / 100,
    // })

    rect = new fabric.Rect({
      top: poly.top,
      left: poly.left,
      width: getEdgeLength(w1),
      height: getEdgeLength(l1),
      angle: getAngle(l1) * (-180 / Math.PI),
      fill: document.getElementById("fillColorPicker").value,
      stroke: document.getElementById("borderColorPicker").value,
      strokeWidth: document.getElementById("hasBorders").checked ? 2 : 0,
      opacity: document.getElementById("opacity").value / 100,
      originY:'center',
      originX: 'center',
    });
    // console.log(poly)
    // canvas.add(poly);
    canvas.add(rect);
    // poly.center();
    canvas.requestRenderAll();

    [xa, ya] = [xb, yb];
    [xd, yd] = [xc, yc];

  }



}