function onkeydown(e) {
    if (e.code === 'AltLeft') {
        canvasChangeCursor("grab");
        // console.log(cursorsHistory);
    }
    if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
    if (ctrlDown && (e.keyCode == cKey)) canvas.getActiveObject() === undefined ? null : Copy();
    if (ctrlDown && (e.keyCode == vKey)) Paste();

    console.log(e.keyCode)

}


function onkeyup(e) {
    if (e.code === 'AltLeft') {
        canvasChangeCursor(cursorsHistory[cursorsHistory.length - 2]);
        // console.log(cursorsHistory);
    }
    if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
    if (e.keyCode == delKey || e.keyCode == backspaceKey) {
        obj = canvas.getActiveObject();
        if (obj) {
            if (obj.type === "activeSelection") {
                obj._objects.forEach(a => {
                    canvas.remove(a);
                });
            } else {
                canvas.remove(canvas.getActiveObject());
            }
            canvas.discardActiveObject().requestRenderAll();
        }

    }

}



function showStatus(e) {
    if (interactiveMode) {
        if (e.code == 'ShiftLeft') {
            var to_be_removed = new Array();
            for (var i = 0; i < canvasZ._objects.length; i++) {
                if (canvasZ._objects[i].type == 'text') {
                    to_be_removed.push(canvasZ._objects[i])
                }
            }
            to_be_removed.forEach(a => canvasZ.remove(a))
            for (var i = 0; i < canvasZ._objects.length; i++) {
                var obj = canvasZ._objects[i];
                if (canvasZ._objects[i].info) {
                    label(canvasZ, obj, obj.info.status, 5)
                }
            }

        }
    }
}

function showID(e) {
    if (interactiveMode) {
        if (e.code) {
            var to_be_removed = new Array();
            for (var i = 0; i < canvasZ._objects.length; i++) {
                if (canvasZ._objects[i].type == 'text') {
                    to_be_removed.push(canvasZ._objects[i])
                } else if (canvasZ._objects[i].ignore){
                    canvasZ._objects[i].set({
                        scale:1,
                        stroke: null,
                        strokeWidth:0
                    })
                }
            }
            canvasZ.requestRenderAll()
            to_be_removed.forEach(a => canvasZ.remove(a))
            for (var i = 0; i < canvasZ._objects.length; i++) {
                var obj = canvasZ._objects[i];
                if (obj.ID && obj.ID !== 0) {
                    label(canvasZ, obj, obj.ID, 5)
                }

            }

        }
    }
}



function showUnitInfo(e) {
    if (interactiveMode) {
        if (e.code == 'MetaRight') {
            var to_be_removed = new Array();
            for (var i = 0; i < canvasZ._objects.length; i++) {
                if (canvasZ._objects[i].type == 'text') {
                    to_be_removed.push(canvasZ._objects[i])
                } else if (canvasZ._objects[i].ignore){
                    canvasZ._objects[i].set({
                        scale:1.25,
                        stroke:'red',
                        strokeWidth:1
                    })
                }
            }
            canvasZ.requestRenderAll()
            to_be_removed.forEach(a => canvasZ.remove(a))
            for (var i = 0; i < canvasZ._objects.length; i++) {
                var obj = canvasZ._objects[i];
                if (canvasZ._objects[i].info && canvasZ._objects[i].info.unit) {
                  var txt = canvasZ._objects[i].info.unit.info;
                    if (txt.length > 0){
                      label(canvasZ, obj, txt,3)  
                    } else{
                      label(canvasZ, obj,"Sample Note" ,3) 
                    }
                    
                }
            }

        }
    }
}



function showUnitNum(e) {
    if (interactiveMode) {
        if (e.code == 'ControlLeft') {
            var to_be_removed = new Array();
            for (var i = 0; i < canvasZ._objects.length; i++) {
                if (canvasZ._objects[i].type == 'text') {
                    to_be_removed.push(canvasZ._objects[i])
                }
            }
            to_be_removed.forEach(a => canvasZ.remove(a))
            for (var i = 0; i < canvasZ._objects.length; i++) {
                var obj = canvasZ._objects[i];
                if (canvasZ._objects[i].info && canvasZ._objects[i].info.unit) {
                  var txt = canvasZ._objects[i].info.unit.num.toString();
                    if (txt.length > 0){
                      label(canvasZ, obj, txt,6)  
                    } else{
                      label(canvasZ, obj,"Empty" ,6) 
                    }
                    
                }
            }

        }
    }
}