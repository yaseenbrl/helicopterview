function generateSpotsData(N){

	var spotStatuses =  ["Closed","Closed","Closed","Closed","Occupied", "Occupied", "Booked", "Booked", "Available", "Available", "Available"]
	var unitStatuses =  ["Loading", "Unloading"]
	var sampleInfo = ["This is a sampleInfo #1", "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","", "This is another, longer sampleInfo that can occupy multiple lines."]
	data = new Object();
	for(var i=1; i<=N; i++){
		_rand = Math.floor(Math.random() * spotStatuses.length)
		data[i.toString()] = {
			status: spotStatuses[_rand],
			unit: (spotStatuses[_rand] === "Occupied" || spotStatuses[_rand] === "Booked") ? {
				status: unitStatuses[Math.floor(Math.random() * unitStatuses.length)],
				num: Math.floor(Math.random() * 100000000),
				info: sampleInfo[Math.floor(Math.random() * sampleInfo.length)],
			} : null,
		}
	}

	return data

}

// canvas.remove(canvas.item(0))
// populateInfo(generateSpotsData(600))
