window.onload = function() {
	  canvasZ = new fabric.Canvas('zoom_canvas');
		canvasZ.setBackgroundColor('red')
		canvasZ.renderAll()


		const channel = new BroadcastChannel("zoomChannel");

    channel.addEventListener("message", e => {
        console.log(e.data);
				// canvas.toJSON(["info", "ID"])
	    	canvasZ.loadFromJSON(e.data);
			 	canvasZ.remove(canvasZ.item(canvasZ._objects.length-1))
				canvasZ.requestRenderAll();
    });

}
